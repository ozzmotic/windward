package edu.neu.madcourse.windward;

import java.util.LinkedList;

import android.content.Context;

import android.util.Log;

public class BreathListener implements AudioClipListener {
    private static final String TAG = "BreathListener";

    private LinkedList<Integer> frequencyHistory;


    public void setRangeThreshold(int rangeThreshold) {
        this.rangeThreshold = rangeThreshold;
    }

    public void setVolThreshold(int volThreshold) {
        this.volThreshold = volThreshold;
    }

    public void setFreqThreshold(int freqThreshold) {
        this.freqThreshold = freqThreshold;
    }

    private int rangeThreshold = 0;
    private int volThreshold = 0;
    private int freqThreshold = 0;

    public static final int DEFAULT_SILENCE_THRESHOLD = 2000;

    private static final boolean DEBUG = false;

    private long prevTime = 0;
    public static int i_freq = 0;
    public static int i_volume = 0;
    public static int i_freqRange = 0;
    private int breathTime = 0;
    private long prevBreathTime = 0;
    private int breathTimeThreshold = 200;
    private float interval = 500;
    private long prevIntervalTime = 0;

    //tutorial toasts
    public static boolean isFirstTimePlay = true;
    public static boolean doTutorialToast_firstMove = false;
    public static boolean readyToCalibrate = false;
    public static boolean showReadyToCalibrateDialog = false;
    public static boolean showHowUseSettings = false;
    public static boolean doneWithSettings = true;
    private boolean showSettingsOnce = true;
    private long prepFirstMoveTime = -1;

    // handle hyperventilation
    private long prevBetweenTime = System.currentTimeMillis();
    private long betweenTime = 3000;
    private long breathBetweenTimeThreshold = 1500;
    public static boolean doHyperVentilationFeedback = false;
    public static boolean doShallowExhaleFeedback = false;
    private boolean heard = false;
    private long prevToastTime = 0;
    private int toastShowCounter = 0;
    private boolean spinOutShip = false;
    private long prevShortBreathTime = 0;
    private int toastDuration = 3500;

    public static int breathInc = 0;
    public static boolean stopShip = false;

    private MovingAvg movingAvgFreq, movingAvgVol;
    private int speedUp = 0;
    private GameScreen gameScreen;
    private int range = 0;

    // start calibration
    // global doCalibration flag
    public static boolean doCalibration = false;
    public static boolean startCalibration = false;
    public static boolean testStartCalibration = false;
    public static int sensitivity = 50;
    public static boolean changedSensitivity = false;
    private float sensitivityAdj = 0.f;

    private boolean doneProcessingCalibrationData = true;

    // calibration stage data to compare with preset threshold
    private int startCalibrationVolThreshold = 1000;
    private int volumePreCalibration = 0;
    private int c_rangeThreshold = 0;
    private int c_volThreshold = 0;
    private int c_freqThreshold = 0;
    private LinkedList<Integer> calibrationVol;
    private LinkedList<Integer> calibrationFreq;
    private LinkedList<Integer> calibrationRange;

    private Context classContext;

    // set from pref
    private boolean setFromPref = false;

    private long prepCalibrationTime;


    public void setDevice(int device) {
        // rise phone
        // 2, 50, 500, 200, 500
        // nexus 7
        // 2, 250, 1500, 200, 500

        if (device == 0) {
            this.freqThreshold = 500;
            this.rangeThreshold = 50;
            this.volThreshold = 3000;
        }

        if (device == 1) {
            this.freqThreshold = 1500;
            this.rangeThreshold = 250;
            this.volThreshold = 500;
        }

        if (device == 2) {
            this.freqThreshold = 500;
            this.rangeThreshold = 50;
            this.volThreshold = 1000;
        }

    }

    public BreathListener(int historySize, int rangeThreshold,
                          int freqThreshold, int breathTime, int volThreshold, GameScreen game) {

        //classContext = GameScreen.context;
        gameScreen = game;

        calibrationVol = new LinkedList<Integer>();
        calibrationFreq = new LinkedList<Integer>();
        calibrationRange = new LinkedList<Integer>();

        calibrationRange.clear();

        frequencyHistory = new LinkedList<Integer>();
        // pre-fill so modification is easy
        for (int i = 0; i < historySize; i++) {
            frequencyHistory.add(Integer.MAX_VALUE);
        }

        this.freqThreshold = freqThreshold;
        this.rangeThreshold = rangeThreshold;
        this.volThreshold = volThreshold;
        this.breathTimeThreshold = breathTime;

        Log.d("initial threshold values", this.volThreshold + " "
                + this.freqThreshold + " " + this.rangeThreshold);

        prevTime = System.currentTimeMillis();
        prevBreathTime = System.currentTimeMillis();
        prevIntervalTime = System.currentTimeMillis();

        // good k values 8, 20, 6, 4
        movingAvgFreq = new MovingAvg(4);
        movingAvgVol = new MovingAvg(4);

        prepCalibrationTime = -1;

    }

    // initial thresholds for settings activity
    private int settingsVolInit = 0;
    private int settingsFreqInit = 0;
    private int settingsRangeInit = 0;

    public BreathListener(int historySize, int rangeThreshold,
                          int freqThreshold, int breathTime, int volThreshold) {

        //classContext = GameScreen.context;
        gameScreen = null;

        settingsVolInit = volThreshold;
        settingsFreqInit = freqThreshold;
        settingsRangeInit = rangeThreshold;

        if (GameScreen.freqThreshold != 0) {
            settingsFreqInit = GameScreen.freqThreshold;
        }
        if (GameScreen.volThreshold != 0) {
            settingsVolInit = GameScreen.volThreshold;
        }
        if (GameScreen.rangeThreshold != 0) {
            settingsRangeInit = GameScreen.rangeThreshold;
        }

        calibrationVol = new LinkedList<Integer>();
        calibrationFreq = new LinkedList<Integer>();
        calibrationRange = new LinkedList<Integer>();

        calibrationRange.clear();

        frequencyHistory = new LinkedList<Integer>();
        // pre-fill so modification is easy
        for (int i = 0; i < historySize; i++) {
            frequencyHistory.add(Integer.MAX_VALUE);
        }

        this.freqThreshold = freqThreshold;
        this.rangeThreshold = rangeThreshold;
        this.volThreshold = volThreshold;
        this.breathTimeThreshold = breathTime;

        Log.d("initial threshold values", this.volThreshold + " "
                + this.freqThreshold + " " + this.rangeThreshold);

        prevTime = System.currentTimeMillis();
        prevBreathTime = System.currentTimeMillis();
        prevIntervalTime = System.currentTimeMillis();

        // good k values 8, 20, 6, 4
        movingAvgFreq = new MovingAvg(4);
        movingAvgVol = new MovingAvg(4);

        prepCalibrationTime = -1;

    }

    @Override
    public boolean heard(short[] audioData, int sampleRate) {
        int frequency = AudioUtil.ZeroCrossing(sampleRate, audioData);
        int volume = (int) AudioUtil.rootMeanSquared(audioData);

        movingAvgFreq.pushValue(frequency);
        frequency = (int) movingAvgFreq.getValue();

        movingAvgVol.pushValue(volume);
        volume = (int) movingAvgVol.getValue();

        // Log.i("first freq", frequency + "");

        frequencyHistory.addFirst(frequency);
        // since history is always full, just remove the last
        frequencyHistory.removeLast();
        range = calculateRange();

        // Log.i("new range", range + " ");

        if (DEBUG) {
            Log.d(TAG, "range: " + range + " freq " + frequency + " loud: "
                    + volume + " history " + frequencyHistory.size());
        }

        if (gameScreen != null) {
            if (changedSensitivity) {

                    gameScreen.storeSensitivityData(sensitivity);

                changedSensitivity = false;
            }
        }

        if (!setFromPref) {

            if (gameScreen != null) {
                if (gameScreen.getVolFromPref() != -1) {
                    volThreshold = gameScreen.getVolFromPref();
                }
                if (gameScreen.getFreqFromPref() != -1) {
                    freqThreshold = gameScreen.getFreqFromPref();
                }
                if (gameScreen.getRangeFromPref() != -1) {
                    rangeThreshold = gameScreen.getRangeFromPref();
                }

                if (sensitivity == 50) {
                    if (gameScreen.getSensitivityFromPref() != -1) {
                        sensitivity = gameScreen.getSensitivityFromPref();
                    }
                } else {
                    gameScreen.storeSensitivityData(sensitivity);
                }

                if (gameScreen.getVolFromPref() != -1 &&
                        gameScreen.getFreqFromPref() != -1 &&
                        gameScreen.getRangeFromPref() != -1) {

                    //if (!doCalibration) {
                    doCalibration = true;
                    isFirstTimePlay = false;
                    showSettingsOnce = false;

                    Log.i(TAG, "DISABLE CALIBRATION");
                    //}
                }


                sensitivityAdj = ((sensitivity - 50) / 100.f);
                Log.i("SenseAdj", sensitivityAdj + " ");

                volThreshold *= (1.f - sensitivityAdj);
                rangeThreshold *= (1.f + sensitivityAdj) * 1.3f;
                freqThreshold *= (1.f + sensitivityAdj) * 1.3f;

                Log.d("pref threshold values", this.volThreshold + " "
                        + this.freqThreshold + " " + this.rangeThreshold + " " + sensitivity);

            }

            setFromPref = true;
        }

        if (gameScreen != null) {
            prepareValuesForCalibration();
        }

        if (System.currentTimeMillis() - prevTime > 150) {
            i_freq = frequency;
            i_volume = volume;
            i_freqRange = range;
            if (gameScreen != null) {
                recordCalibrationData();
            }
            prevTime = System.currentTimeMillis();
        }
        if (gameScreen != null) {
            processCalibrationData();
        }

        if (doCalibration && showSettingsOnce && !MainMenu.notFirstRun) {
            if (prepFirstMoveTime == -1) {
                prepFirstMoveTime = System.currentTimeMillis();
            }

            if (System.currentTimeMillis() - prepFirstMoveTime > 5000) {
                showHowUseSettings = true;
                //doneWithSettings = false;
                showSettingsOnce = false;
            }
        }

        // for help/settings
        if (gameScreen == null) {

            if (BreathListener.changedSensitivity) {

                volThreshold = settingsVolInit;
                rangeThreshold = settingsRangeInit;
                freqThreshold = settingsFreqInit;

                sensitivityAdj = ((sensitivity - 50) / 100.f);
                volThreshold *= (1.f - sensitivityAdj);
                rangeThreshold *= (1.f + sensitivityAdj);
                freqThreshold *= (1.f + sensitivityAdj);

                Log.d("Settings vol", volThreshold + "");
                Log.d("Settings range", rangeThreshold + "");
                Log.d("Settings freq", freqThreshold + "");

                BreathListener.changedSensitivity = false;
            }

            if (i_volume > volThreshold && i_freqRange < rangeThreshold && i_freq < freqThreshold) {
                Settings.heardBreath = true;
            }
        }

        if (gameScreen != null) {
            if (i_volume > volThreshold && i_freqRange < rangeThreshold && i_freq < freqThreshold
                    && (doCalibration || readyToCalibrate) && doneWithSettings &&
                    GameScreen.gameView.doneWithIntroDialog) {

                breathTime = (int) (System.currentTimeMillis() - prevBreathTime);

                if (breathTime > breathTimeThreshold) {

                    // hyperventilation
                    if (betweenTime >= breathBetweenTimeThreshold) {

                        // allow ship to move from edge condition
                        stopShip = false;
                        spinOutShip = false;

                        speedUp += 1;
                        //Log.d("breathTime", breathTime + " " + betweenTime);

                        if (interval > 140.f) {
                            if (gameScreen.doPopupWindowOnce) {
                                interval -= (20.f + speedUp);
                            }
                        }

                        if (System.currentTimeMillis() - prevIntervalTime > interval) {
                            breathInc = breathInc + 1;

                            if (isFirstTimePlay && !doTutorialToast_firstMove && breathInc > 5) {
                                Log.d(TAG, "do first tutorial toast");
                                doTutorialToast_firstMove = true;
                                isFirstTimePlay = false;
                            }

                            if (gameScreen != null) {
                                if (gameScreen.doPopupWindowOnce) {
                                    gameScreen.moveShip();
                                }
                            }

                            // Log.d(TAG, " " + breathInc);
                            heard = true;
                            prevIntervalTime = System.currentTimeMillis();
                        }

                    }

                    //prevBetweenTime = System.currentTimeMillis();
                }

                // hyperventilation status
                // only show after first successful exhale
                // only show toast max of three time
                if (heard && (betweenTime < breathBetweenTimeThreshold) && doCalibration && !isFirstTimePlay) {
                    // prevent false positives
                    // 200
                    if (betweenTime > 500 && breathTime > breathTimeThreshold) {
                        Log.d("hyperventilation", "hyperventilation! " + betweenTime);

                        if (gameScreen != null)
                            gameScreen.spinOutShip();


                        spinOutShip = true;

                        if (System.currentTimeMillis() - prevToastTime > toastDuration && toastShowCounter < 3) {

                            if (System.currentTimeMillis() - prevShortBreathTime > toastDuration) {
                                // trigger hyperventilation feedback
                                doHyperVentilationFeedback = true;
                            }

                            if (toastShowCounter < 3) {
                                //toastShowCounter++;
                            }

                            //prevBetweenTime = System.currentTimeMillis();
                            prevToastTime = System.currentTimeMillis();
                        }
                    }

                }

                prevBetweenTime = System.currentTimeMillis();

            } else if (doCalibration || readyToCalibrate) {

                if (breathTime <= breathTimeThreshold && breathTime != 0 && !isFirstTimePlay) {
                    // prevent false positives
                    if (breathTime > 100) {
                        Log.d("shallow breath time", "shallow breath " + breathTime);

                        if (System.currentTimeMillis() - prevToastTime > toastDuration) {
                            if (System.currentTimeMillis() - prevShortBreathTime > toastDuration) {
                                // display toast about shallow breath
                                //Log.i("shallow breath", "shallowBreath");
                                doShallowExhaleFeedback = true;

                                prevShortBreathTime = System.currentTimeMillis();
                            }
                        }
                    }
                }

                if (breathTime != 0) {
                    if (spinOutShip) {

                        if (gameScreen != null)
                            gameScreen.shipCoastingReOrientation();

                        Log.i(TAG, "reorient");
                    }
                    breathTime = 0;
                }

                prevBreathTime = System.currentTimeMillis();
                speedUp = 0;
                if (betweenTime < Long.MAX_VALUE) {
                    betweenTime = Math.abs(System.currentTimeMillis() - prevBetweenTime);
                }

                // stopShip prevent ship movement from edge condition
                if (interval < 500 && !stopShip) {
                    if (spinOutShip) {
                        interval += 10.f;
                    } else {
                        if (gameScreen.doPopupWindowOnce) {
                            interval += 2.f; //5.f
                        } else {
                            interval += 7.f;
                        }

                    }
                    if (System.currentTimeMillis() - prevIntervalTime > interval) {
                        //breathInc = breathInc + 1;
                        breathInc = 0;

                        if (gameScreen != null)
                            gameScreen.moveShip();


                        // Log.d(TAG, " " + breathInc);
                        prevIntervalTime = System.currentTimeMillis();
                    }
                }

            }
        }

            return heard;
        }


    private int calculateRange() {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (Integer val : frequencyHistory) {
            if (val >= max) {
                max = val;
            }

            if (val < min) {
                min = val;
            }
        }

		/*
         * if (DEBUG) { StringBuilder sb = new StringBuilder(); for (Integer val
		 * : frequencyHistory) { sb.append(val).append(" "); } Log.d(TAG,
		 * sb.toString() + " [" + (max - min) + "]"); }
		 */

        return Math.abs(max - min);
    }

    // do calibration again
    public static void doCalibration() {
        doCalibration = false;
        testStartCalibration = false;
    }

    private void prepareValuesForCalibration() {

        if (prepCalibrationTime == -1 && !doCalibration) {
            prepCalibrationTime = System.currentTimeMillis();
        }

        // QQQ delay the calibration time on first launch
        if (!doCalibration && !testStartCalibration) {
            if (System.currentTimeMillis() - prepCalibrationTime > 3500 && range < 10000) {

                if (GameScreen.gameView.doneWithIntroDialog) {
                    BreathListener.testStartCalibration = true;
                    BreathListener.showReadyToCalibrateDialog = true;

                    Log.i("start calibration", "toast");
                }

            }
        }

    }

    private void recordCalibrationData() {
        if (!doCalibration) {


            if (!testStartCalibration && !startCalibration) {
                // set before calibration
                volumePreCalibration = i_volume;
                // Log.i("vol pre calibration", "volume pre calibration");
            }

            // start Calibration is allowed to be true only if current vol is at
            // least % greater than
            // previous vol
            if (testStartCalibration && !startCalibration) {
                if (i_volume > /*startCalibrationVolThreshold*/ volumePreCalibration * 10.f) {

                    // ready to calibrate dialog
                    if (readyToCalibrate) {
                        startCalibration = true;
                        Log.i("vol pre calibration", "start calibration");
                    }
                }
            }

            if (startCalibration) {

                Log.i("vol pre calibration", "adding calibration values");
                // save a list of calibration breathing values for vol, freq,
                // freq range
                doneProcessingCalibrationData = false;
                calibrationVol.add(i_volume);
                calibrationFreq.add(i_freq);
                calibrationRange.add(i_freqRange);

                if (calibrationVol.size() > 10) {
                    startCalibration = false;
                }
            }
        }
    }

    private void processCalibrationData() {
        if (!doCalibration) {
            if (!startCalibration) {
                if (!doneProcessingCalibrationData) {
                    // calculate the average of each parameter

                    // VOLUME
                    if (!calibrationVol.isEmpty()) {
                        for (int i = 0; i < calibrationVol.size(); i++) {
                            c_volThreshold += calibrationVol.get(i);
                        }
                        c_volThreshold = c_volThreshold / calibrationVol.size();
                        calibrationVol.clear();
                    }

                    // FREQUENCY
                    if (!calibrationFreq.isEmpty()) {
                        for (int i = 0; i < calibrationFreq.size(); i++) {
                            c_freqThreshold += calibrationFreq.get(i);
                        }
                        c_freqThreshold = c_freqThreshold
                                / calibrationFreq.size();
                        calibrationFreq.clear();
                    }

                    // RANGE
                    if (!calibrationRange.isEmpty()) {
                        for (int i = 0; i < calibrationRange.size(); i++) {
                            c_rangeThreshold += calibrationRange.get(i);
                            Log.i("range debug", calibrationRange.get(i)
                                    + " size " + calibrationRange.size());
                        }
                        c_rangeThreshold = Math.abs(c_rangeThreshold
                                / calibrationRange.size()); // /
                        // calibrationRange.size();
                        calibrationRange.clear();
                    }

                    // sensitivity -0.5 -> 0.5
                    // apply sensitivity adjustment
                    // vol
                    // * (1-sensitivity)

                    sensitivityAdj = ((sensitivity - 50) / 100.f);
                    Log.i("SenseAdj", sensitivityAdj + " ");

                    volThreshold = (int) (c_volThreshold);

                    // freq and rang
                    // * (1+sensitivity)
                    freqThreshold = (int) (c_freqThreshold);
                    rangeThreshold = (int) (c_rangeThreshold);

                    volThreshold *= (1.f - sensitivityAdj);
                    rangeThreshold *= (1.f + sensitivityAdj);
                    freqThreshold *= (1.f + sensitivityAdj);

                    Log.d("Calibrated threshold values", volThreshold + " "
                            + freqThreshold + " " + rangeThreshold);

                    // storeCalibrationData(classContext);

                    if (gameScreen != null) {
                        gameScreen.storeCalibrationData(volThreshold,
                                freqThreshold, rangeThreshold);
                    }

                    if (gameScreen != null) {
                        gameScreen.storeSensitivityData(sensitivity);
                    }

                    // debugging
                    //int tvol = gameScreen.getVolFromPref();

                    testStartCalibration = false;
                    doneProcessingCalibrationData = true;
                    doCalibration = true;
                }
            }
        }
    }


}