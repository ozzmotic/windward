package edu.neu.madcourse.windward;

import java.io.IOException;
import java.io.PrintWriter;

import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.Environment;
import android.util.Log;

public class AudioUtil
{
    private static final String TAG = "AudioUtil";

    public static boolean hasMicrophone(Context context)
    {
        return context.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_MICROPHONE);
    }

    public static boolean isSilence(short [] data)
    {
        boolean silence = false;
        int RMS_SILENCE_THRESHOLD = 2000;
        if (rootMeanSquared(data) < RMS_SILENCE_THRESHOLD)
        {
            silence = true;
        }
        return silence;
    }

    public static int ZeroCrossing(int sampleRate, short[] audioData) {
        int numSamples = audioData.length;
        int numCrossing = 0;
        for (int p = 0; p < numSamples - 1; p++) {
            if ((audioData[p] > 0 && audioData[p + 1] <= 0) || (audioData[p] < 0 && audioData[p + 1] >= 0)) {
                numCrossing++;
            }
        }
        float numSecondsRecorded = (float) numSamples / (float) sampleRate;
        float numCycles = numCrossing / 2;
        float frequency = numCycles / numSecondsRecorded;
        return (int) frequency;
    }

    public static double rootMeanSquared(short[] nums)
    {
        double ms = 0;
        for (int i = 0; i < nums.length; i++)
        {
            ms += nums[i] * nums[i];
        }
        ms /= nums.length;
        return Math.sqrt(ms);
    }

    public static int countZeros(short [] audioData)
    {
        int numZeros = 0;

        for (int i = 0; i < audioData.length; i++)
        {
            if (audioData[i] == 0)
            {
                numZeros++;
            }
        }

        return numZeros;
    }

    public static double secondsPerSample(int sampleRate)
    {
        return 1.0/(double)sampleRate;
    }

    public static int numSamplesInTime(int sampleRate, float seconds)
    {
        return (int)((float)sampleRate * (float)seconds);
    }

    public static void outputData(short [] data, PrintWriter writer)
    {
        for (int i = 0; i < data.length; i++)
        {
            writer.println(String.valueOf(data[i]));
        }
        if (writer.checkError())
        {
            Log.w(TAG, "Error writing sensor event data");
        }
    }
}