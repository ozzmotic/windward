package edu.neu.madcourse.windward;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.graphics.Canvas;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    public boolean stopDraw = false;

    private Paint backgroundPaint;
    private Paint dark;
    private Paint distPaint;

    private float mWidth;
    private float mHeight;
    private ViewThread mThread;

    private final int OBJ_SHIP = 0;
    private final int OBJ_ISLAND = 1;
    private final int OBJ_BOTTLE = 2;
    private final int OBJ_WAVE = 3;
    private final int OBJ_WHALE = 4;

    private WorldObject[][] grid;
    public int temp;
    private int tempLocationX;
    private List<Integer> bottleList;
    private List<Integer> objectiveLocationsX;
    private List<Integer> objectiveLocationsY;
    private float objectiveAngle = -90;
    private int targetRow;
    private int targetCol;
    private float distance;
    private int distInt;
    private Typeface distFont;

    private int gridLength;
    private int gridWidth;
    private int tileSide;
    private int shipRow;
    private int shipCol;
    public boolean canInteract = false;
    private boolean[] gotBottles = new boolean[12];
    private boolean inExploreMode = false;
    public int bottlesCollected = 0;
    public int currentQuest = 0;
    public boolean displayBottleMessage = false;
    public boolean displayQuestMessage = false;
    public static boolean displayIslandImage = false;
    public boolean displayIntroDialog = false;
    public boolean displayAlreadyVisitedDialog = false;

    private int mapSize;
    private Rect currentWindow;
    private int ship_orientation = 0; //0 = up, 1 = right, 2 = down, 3 = left
    private int prev_orientation = -1;
    private long prevSpinTime = 0;

    public int waveStageC = 0;
    public int waveStageP = 1;
    public int introTextCounter = 0;

    private Bitmap compassBitmap;

    public Boolean showIntroDialog = false;
    public Boolean doneWithIntroDialog = false;

    public boolean gettingCloserToTarget = false;
    private boolean showTargetCloseToast = false;
    public boolean preparedCompass = true;

    private int prevDistToTarget = 0;
    public boolean gettingFarFromTarget = false;
    private boolean showTargetAwayToast = false;
    public boolean firstTimeDistanceToast = false;

    public GameView(Context context, float width, float height) {
        super(context);
        getHolder().addCallback(this);

        mWidth = width;
        mHeight = height;

        mapSize = 500;
        gridLength = 14;
        gridWidth = 14;
        tileSide = (int) mHeight / (gridLength + 4);
        bottleList = Arrays.asList(495, 200, 260, 120, 130, 220, 475, 325, 450, 490, 250, 270);
        objectiveLocationsX = Arrays.asList(485, 400, 400, 210, 210, 125, 480); //first island 495
        objectiveLocationsY = Arrays.asList(9, 300, 300, 250, 250, 450, 455);

        //Initialize map grid
        grid = new WorldObject[mapSize][mapSize];

        //Paints
        backgroundPaint = new Paint();
        backgroundPaint.setColor(getResources().getColor(R.color.water));


        dark = new Paint();
        dark.setColor(getResources().getColor(R.color.black));

        //Place ship at starting location
        shipRow = 498;
        shipCol = 9;

        targetCol = -1;
        targetRow = -1;

        mThread = new ViewThread(this);

        //load compass into bitmap
        if (compassBitmap != null) {
            compassBitmap.recycle();
        }
        compassBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.compass);
    }

    public void setDistFont(Typeface f) {
        distFont = f;
        distPaint = new Paint();
        distPaint.setTypeface(distFont);
        distPaint.setColor(getResources().getColor(R.color.white));
        distPaint.setTextSize(18.f);
        distPaint.setTextAlign(Paint.Align.CENTER);
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!mThread.isAlive()) {
            mThread = new ViewThread(this);
            mThread.setRunning(true);
            mThread.start();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        mWidth = width;
        mHeight = height;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        //Stop thread
        if (mThread.isAlive()) {
            mThread.setRunning(false);
        }
    }

    public void clearWaves() {

        for (int x = shipRow - 100; x < shipRow + 100; x++) {
            for (int y = shipCol - 100; y < shipCol + 100; y++) {
                if (x >= 0 && y >= 0 && x < mapSize && y < mapSize) {
                    if (grid[x][y] != null) {

                        if (grid[x][y].getType() == 3) {
                            grid[x][y].place = false;
                        }

                        if (grid[x][y].getType() == 3 && grid[x][y].opacity <= 5) {
                            grid[x][y] = null;
                        }
                    }
                }
            }
        }
    }

    public void placeWaves() {
        Random r = new Random();
        for (int x = 0; x < mapSize; x = x + r.nextInt(10)) {
            for (int y = 0; y < mapSize; y = y + r.nextInt(10)) {
                if (grid[x][y] != null) continue;

                grid[x][y] = new WorldObject(tileSide, OBJ_WAVE);
                //grid[x][y].waveStage = waveStageP;
                grid[x][y].place = true;
            }
        }
    }

    public void placePlayerWaves() {

        Random r = new Random();
        for (int x = shipRow - 100; x < shipRow + 100; x = x + r.nextInt(7)) {
            for (int y = shipCol - 100; y < shipCol + 100; y = y + r.nextInt(7)) {
                if (x >= 0 && y >= 0 && x < mapSize && y < mapSize) {
                    if (grid[x][y] == null) {
                        grid[x][y] = new WorldObject(tileSide, OBJ_WAVE);
                        grid[x][y].place = true;
                    }
                }
            }
        }
    }

    public void placeShip() {
        if (grid[shipRow][shipCol] != null) {
            grid[shipRow][shipCol].releaseDrawable();
        }

        grid[shipRow][shipCol] = new WorldObject(tileSide, OBJ_SHIP);
    }

    public void placeIslands() {
        grid[objectiveLocationsX.get(0)][objectiveLocationsY.get(0)] = new WorldObject(tileSide, OBJ_ISLAND); //first island
        grid[objectiveLocationsX.get(1)][objectiveLocationsY.get(1)] = new WorldObject(tileSide, OBJ_ISLAND); //shipwrecked
        grid[objectiveLocationsX.get(3)][objectiveLocationsY.get(3)] = new WorldObject(tileSide, OBJ_ISLAND); //treasure
        grid[objectiveLocationsX.get(5)][objectiveLocationsY.get(5)] = new WorldObject(tileSide, OBJ_ISLAND); //crossroads island
        grid[objectiveLocationsX.get(6)][objectiveLocationsY.get(6)] = new WorldObject(tileSide, OBJ_WHALE); //whale
    }

    public void startExplorationMode() {
        inExploreMode = true;

        //Remove quest islands
        grid[objectiveLocationsX.get(0)][objectiveLocationsY.get(0)] = null;
        grid[objectiveLocationsX.get(1)][objectiveLocationsY.get(1)] = null;
        grid[objectiveLocationsX.get(3)][objectiveLocationsY.get(3)] = null;
        grid[objectiveLocationsX.get(5)][objectiveLocationsY.get(5)] = null;

        // causing ship location error
        //grid[objectiveLocationsX.get(6)][objectiveLocationsY.get(6)] = null;

        //Add exploration islands

        grid[480][8] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[51][9] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[102][20] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[152][250] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[202][420] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[251][9] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[310][20] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[355][250] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[409][420] = new WorldObject(tileSide, OBJ_ISLAND);

        grid[232][81] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[215][333] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[113][175] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[361][33] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[411][211] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[197][167] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[378][35] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[416][494] = new WorldObject(tileSide, OBJ_ISLAND);

        grid[20][81] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[207][333] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[55][175] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[480][33] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[471][211] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[373][167] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[425][35] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[102][494] = new WorldObject(tileSide, OBJ_ISLAND);

        grid[18][335] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[485][122] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[230][473] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[375][75] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[498][311] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[451][130] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[97][205] = new WorldObject(tileSide, OBJ_ISLAND);
        grid[26][112] = new WorldObject(tileSide, OBJ_ISLAND);
    }

    public void placeBottles() {
        //Place 12 bottles (if continuing game, check to see if player has collected any)
        for (int n = 0; n < gotBottles.length; n++) {
            if (!gotBottles[n]) {
                switch (n) {
                    case 0:
                        if (grid[495][25] != null) {
                            grid[495][25].releaseDrawable();
                        }
                        grid[495][25] = new WorldObject(tileSide, OBJ_BOTTLE);
                        break;
                    case 1:
                        if (grid[200][30] != null) {
                            grid[200][30].releaseDrawable();
                        }
                        grid[200][30] = new WorldObject(tileSide, OBJ_BOTTLE);
                        break;
                    case 2:
                        if (grid[260][125] != null) {
                            grid[260][125].releaseDrawable();
                        }
                        grid[260][125] = new WorldObject(tileSide, OBJ_BOTTLE);
                        break;
                    case 3:
                        if (grid[120][200] != null) {
                            grid[120][200].releaseDrawable();
                        }
                        grid[120][200] = new WorldObject(tileSide, OBJ_BOTTLE);
                        break;
                    case 4:
                        if (grid[130][200] != null) {
                            grid[130][200].releaseDrawable();
                        }
                        grid[130][200] = new WorldObject(tileSide, OBJ_BOTTLE);
                        break;
                    case 5:
                        if (grid[220][433] != null) {
                            grid[220][433].releaseDrawable();
                        }
                        grid[220][433] = new WorldObject(tileSide, OBJ_BOTTLE);
                        break;
                    case 6:
                        if (grid[475][345] != null) {
                            grid[475][345].releaseDrawable();
                        }
                        grid[475][345] = new WorldObject(tileSide, OBJ_BOTTLE);
                        break;
                    case 7:
                        if (grid[325][452] != null) {
                            grid[325][452].releaseDrawable();
                        }
                        grid[325][452] = new WorldObject(tileSide, OBJ_BOTTLE);
                        break;
                    case 8:
                        if (grid[450][250] != null) {
                            grid[450][250].releaseDrawable();
                        }
                        grid[450][250] = new WorldObject(tileSide, OBJ_BOTTLE);
                        break;
                    case 9:
                        if (grid[490][75] != null) {
                            grid[490][75].releaseDrawable();
                        }
                        grid[490][75] = new WorldObject(tileSide, OBJ_BOTTLE);
                        break;
                    case 10:
                        if (grid[250][30] != null) {
                            grid[250][30].releaseDrawable();
                        }
                        grid[250][30] = new WorldObject(tileSide, OBJ_BOTTLE);
                        break;
                    case 11:
                        if (grid[270][300] != null) {
                            grid[270][300].releaseDrawable();
                        }
                        grid[270][300] = new WorldObject(tileSide, OBJ_BOTTLE);
                        break;
                }
            }
        }
    }

    public void spinOutShip() {
        if (prev_orientation == -1) {
            prev_orientation = ship_orientation;
        }

        if (System.currentTimeMillis() - prevSpinTime > 200) {
            prev_orientation = (prev_orientation + 1) % 4;
            prevSpinTime = System.currentTimeMillis();
        }

        grid[shipRow][shipCol].rotate(prev_orientation);
    }

    public void shipCoastingReOrientation() {
        grid[shipRow][shipCol].rotate(ship_orientation);
    }

    private boolean updateMoveShip = false;
    public boolean showEndWorldToast = false;
    public void moveShip() {
        if (stopDraw == false) {
            updateMoveShip = true;

            if (prev_orientation != -1) {
                prev_orientation = -1;
            }

            switch (ship_orientation) { //Do nothing if reached edge and trying to go further
                case 0: //move up
                    if (shipRow == 0) {
                        BreathListener.stopShip = true;
                        showEndWorldToast = true;
                        return;
                    }
                    break;
                case 1: //move right
                    if (shipCol == mapSize - 1) {
                        BreathListener.stopShip = true;
                        showEndWorldToast = true;
                        return;
                    }
                    break;
                case 2: //move down
                    if (shipRow == mapSize - 1) {
                        BreathListener.stopShip = true;
                        showEndWorldToast = true;
                        return;
                    }
                    break;
                case 3: //move left
                    if (shipCol == 0) {
                        BreathListener.stopShip = true;
                        showEndWorldToast = true;
                        return;
                    }
                    break;
            }

            WorldObject ship = grid[shipRow][shipCol];
            grid[shipRow][shipCol] = null;

            switch (ship_orientation) {
                case 0: //move up
                    if (grid[shipRow - 1][shipCol] != null) {
                        switch (grid[shipRow - 1][shipCol].getType()) {
                            case OBJ_ISLAND:
                                canInteract = true;
                                temp = OBJ_ISLAND;
                                tempLocationX = shipRow - 1;
                                break;
                            case OBJ_BOTTLE:
                                canInteract = true;
                                temp = OBJ_BOTTLE;
                                tempLocationX = shipRow - 1;
                                break;
                            case OBJ_WHALE:
                                canInteract = true;
                                temp = OBJ_WHALE;
                                tempLocationX = shipRow - 1;
                                break;
                            case OBJ_WAVE:
                                canInteract = false;
                                temp = OBJ_WAVE;
                                break;
                            default:
                                canInteract = false;
                                //temp = 99;
                                break;
                        }
                    }
                    grid[shipRow - 1][shipCol] = ship;
                    shipRow--;
                    break;
                case 1: //move right
                    if (grid[shipRow][shipCol + 1] != null) {
                        switch (grid[shipRow][shipCol + 1].getType()) {
                            case OBJ_ISLAND:
                                canInteract = true;
                                temp = OBJ_ISLAND;
                                tempLocationX = shipRow;
                                break;
                            case OBJ_BOTTLE:
                                canInteract = true;
                                temp = OBJ_BOTTLE;
                                tempLocationX = shipRow;
                                break;
                            case OBJ_WHALE:
                                canInteract = true;
                                temp = OBJ_WHALE;
                                tempLocationX = shipRow;
                                break;
                            case OBJ_WAVE:
                                canInteract = false;
                                temp = OBJ_WAVE;
                                break;
                            default:
                                canInteract = false;
                                //temp = 99;
                                break;
                        }
                    }
                    grid[shipRow][shipCol + 1] = ship;
                    shipCol++;
                    break;
                case 2: //move down
                    if (grid[shipRow + 1][shipCol] != null) {
                        switch (grid[shipRow + 1][shipCol].getType()) {
                            case OBJ_ISLAND:
                                canInteract = true;
                                temp = OBJ_ISLAND;
                                tempLocationX = shipRow + 1;
                                break;
                            case OBJ_BOTTLE:
                                canInteract = true;
                                temp = OBJ_BOTTLE;
                                tempLocationX = shipRow + 1;
                                break;
                            case OBJ_WAVE:
                                canInteract = false;
                                temp = OBJ_WAVE;
                                break;
                            case OBJ_WHALE:
                                canInteract = true;
                                temp = OBJ_WHALE;
                                tempLocationX = shipRow + 1;
                                break;
                            default:
                                canInteract = false;
                                //temp = 99;
                                break;
                        }
                    }
                    grid[shipRow + 1][shipCol] = ship;
                    shipRow++;
                    break;
                case 3: //move left
                    if (grid[shipRow][shipCol - 1] != null) {
                        switch (grid[shipRow][shipCol - 1].getType()) {
                            case OBJ_ISLAND:
                                canInteract = true;
                                temp = OBJ_ISLAND;
                                tempLocationX = shipRow;
                                break;
                            case OBJ_BOTTLE:
                                canInteract = true;
                                temp = OBJ_BOTTLE;
                                tempLocationX = shipRow;
                                break;
                            case OBJ_WAVE:
                                canInteract = false;
                                temp = OBJ_WAVE;
                                break;
                            case OBJ_WHALE:
                                canInteract = true;
                                temp = OBJ_WHALE;
                                tempLocationX = shipRow;
                                break;
                            default:
                                canInteract = false;
                                //temp = 99;
                                break;
                        }
                    }
                    grid[shipRow][shipCol - 1] = ship;
                    shipCol--;
                    break;
            }

            if (temp == OBJ_ISLAND) {
                placeIslands();
                placeShip();
                grid[shipRow][shipCol].rotate(ship_orientation);
            }

            if (temp == OBJ_WHALE) {
                Log.d("Ship coordinates from moveship", shipRow + " " + shipCol);
                placeShip();
                grid[shipRow][shipCol].rotate(ship_orientation);
            }

            updateWindow();
            setAllBoundsInWindow();
            prepCompass();
        }
    }

    public void prepCompass() {
        if (currentQuest < 7 && currentQuest > 0) {
            objectiveAngle = getAngle(objectiveLocationsX.get(currentQuest), objectiveLocationsY.get(currentQuest));
            targetRow = objectiveLocationsX.get(currentQuest);
            targetCol = objectiveLocationsY.get(currentQuest);
        }
    }

    public void steerShipLeft() {
        //set flag for moveShip
        switch (ship_orientation) { //0 = up, 1 = right, 2 = down, 3 = left
            case 0:
                ship_orientation = 3;
                break;
            case 3:
                ship_orientation = 2;
                break;
            case 2:
                ship_orientation = 1;
                break;
            case 1:
                ship_orientation = 0;
                break;
        }

        //Update graphic
        if (grid[shipRow][shipCol] != null) {
            grid[shipRow][shipCol].rotate(ship_orientation);
        }

    }

    public void steerShipRight() {
        //set flag for moveShip
        switch (ship_orientation) { //0 = up, 1 = right, 2 = down, 3 = left
            case 0:
                ship_orientation = 1;
                break;
            case 3:
                ship_orientation = 0;
                break;
            case 2:
                ship_orientation = 3;
                break;
            case 1:
                ship_orientation = 2;
                break;
        }

        //Update graphic
        if (grid[shipRow][shipCol] != null) {
            grid[shipRow][shipCol].rotate(ship_orientation);
        }
    }

    public void interactWithWorldObject(int obj) {
        canInteract = false;
        //Determine the object
        if (obj == OBJ_BOTTLE) {
            gotBottles[bottleList.indexOf(tempLocationX)] = true;

            //display the bottle message
            displayBottleMessage = true;
        } else if (obj == OBJ_ISLAND || obj == OBJ_WHALE) {
            //currentQuest: 0: first island, 1-2: island 2, 3-4: island 3, 5: island 4, 6: whale
            if (currentQuest < 7) {
                if (tempLocationX == objectiveLocationsX.get(currentQuest)) {
                    displayQuestMessage = true;
                } else {
                    displayAlreadyVisitedDialog = true;
                }
            } else {
                // Get image from instagram
                IslandImage.getInstagramImage = true;
                Log.i("Instagram test", "called once?");
            }
        }
    }

    public void setAllBoundsInWindow() {
        if (!stopDraw) {
            synchronized (grid) {
                for (int x = 0; x < gridWidth; x++) {
                    for (int y = 0; y < gridLength; y++) {
                        if (currentWindow.top + x < 0 || currentWindow.top + x >= mapSize) {
                            continue;
                        }
                        if (currentWindow.left + y < 0 || currentWindow.left + y >= mapSize) {
                            continue;
                        }

                        try {
                            if (grid[currentWindow.top + x][currentWindow.left + y] != null) {
                                grid[currentWindow.top + x][currentWindow.left + y].setBounds(x * tileSide, y * tileSide);
                            }
                        } catch (NullPointerException e) {
                            Log.w("onDraw", "Caught nullpointerexception from setAllBounds...");
                            Log.w("onDraw", "x = " + currentWindow.top + " + " + x + ", y = " + currentWindow.left + " + " + y);
                        }
                    }
                }
            }
        }
    }

    public int getShipRow() {
        return shipRow;
    }

    public int getShipCol() {
        return shipCol;
    }

    public boolean[] getGotBottles() {
        return gotBottles;
    }

    public float getAngle(int objectiveY, int objectiveX) {
        float angle = (float) Math.toDegrees(Math.atan2(objectiveY - shipRow, objectiveX - shipCol));
        return angle;
    }

    public int getShipOrientation() { return ship_orientation; }

    public void setShipRow(int s) {
        shipRow = s;
    }

    public void setShipCol(int s) {
        shipCol = s;
    }

    public void setGotBottles(boolean b, int index) {
        gotBottles[index] = b;
    }

    public void setShipOrientation( int o ) {
        ship_orientation = o;
    }

    public void updateWindow() {
        if (!stopDraw) {
            int left = Math.round(shipCol - gridWidth / 2);
            int bottom = Math.round(shipRow + 2);
            int top = bottom - (gridLength + 1);
            int right = Math.round(shipCol + gridWidth / 2);

            if (left < 0) {
                left = 0;
            }
            if (right >= mapSize) {
                right = mapSize - 1;
            }
            if (top < 0) {
                top = 0;
            }
            if (bottom >= mapSize) {
                bottom = mapSize - 1;
            }

            currentWindow = new Rect(left, top, right, bottom);
        }
    }

    public void doDraw(Canvas canvas) {

        if (!stopDraw) {
            if (showIntroDialog) {
                this.displayIntroDialog = true;
                showIntroDialog = false;
            }

            if (temp == OBJ_WHALE) {
                if (grid[shipRow][shipCol] == null) {
                    Log.d("Ship coordinates from onDraw", shipRow + " " + shipCol);
                    placeShip();
                    grid[shipRow][shipCol].rotate(ship_orientation);
                }
            }
            canvas.drawPaint(backgroundPaint);

            //Draw every object in the visible window
            if (grid != null) {
                synchronized (grid) {
                    for (int x = 0; x < gridWidth; ++x) {
                        for (int y = 0; y < gridLength; ++y) {

                            if (currentWindow.top + x < 0 || currentWindow.top + x >= mapSize)
                                continue;
                            if (currentWindow.left + y < 0 || currentWindow.left + y >= mapSize)
                                continue;
                            if (grid != null) {
                                if (grid[currentWindow.top + x][currentWindow.left + y] == null)
                                    continue;
                            }

                            try {
                                if (grid != null) {
                                    grid[currentWindow.top + x][currentWindow.left + y].doDraw(canvas);
                                }
                            } catch (NullPointerException e) {
                                Log.w("onDraw", "Caught nullpointerexception");
                                Log.w("onDraw", "x = " + currentWindow.top + " + " + x + ", y = " + currentWindow.left + " + " + y);
                            }
                        }
                    }
                }
            }

            // COMPASS
            if (!inExploreMode) {
                // calculate distance to target
                // skip sqrt colculation for speed
                if (targetRow != -1 && currentQuest > 0) {
                    if (hasValidBitmap()) {
                        canvas.save(Canvas.MATRIX_SAVE_FLAG);
                        canvas.translate(mWidth - compassBitmap.getWidth() * 0.08f - 2.f, compassBitmap.getWidth() * 0.08f);
                        canvas.rotate(objectiveAngle - 270);
                        canvas.scale(0.16f, 0.16f);
                        canvas.drawBitmap(compassBitmap, compassBitmap.getWidth() * -0.5f, compassBitmap.getHeight() * -0.5f, null);
                        canvas.restore();

                        distance = (float) Math.sqrt(((targetRow - shipRow) * (targetRow - shipRow)) + ((targetCol - shipCol) * (targetCol - shipCol))); //* 0.005f;
                        distInt = (int) (distance);

                        if (preparedCompass) {
                            if (distInt > 50) {
                                showTargetCloseToast = true;
                                prevDistToTarget = distInt;
                                showTargetAwayToast = true;
                                Log.i("RESET getting closer", "RESET getting closer");
                                preparedCompass = false;
                            }
                        }

                        if (MainMenu.isFirstRunTime) {
                            if (distInt - prevDistToTarget <= -10) {
                                firstTimeDistanceToast = true;
                                MainMenu.isFirstRunTime = false;
                            }
                        }

                        if (distInt < 50 && showTargetCloseToast) {
                            gettingCloserToTarget = true;
                            showTargetCloseToast = false;
                        }

                        if (distInt - prevDistToTarget > 10 && showTargetAwayToast) {
                            // you are moving away warning
                            gettingFarFromTarget = true;
                            showTargetAwayToast = false;
                        }

                        canvas.save(Canvas.MATRIX_SAVE_FLAG);
                        canvas.translate((mWidth - compassBitmap.getWidth() * 0.08f), (compassBitmap.getWidth() * 0.08f) + 5.f);
                        canvas.drawText(String.valueOf(distInt), 0, 0, distPaint);
                        canvas.restore();
                    }
                }
            }

        }

    }

    public void recycleBitmap() {
        compassBitmap.recycle();
    }

    private boolean hasValidBitmap() {
        return compassBitmap != null && !compassBitmap.isRecycled();
    }

    private int recycleCount = 0;
    public void recycleDrawables() {
        if (grid != null) {
            for (int x = 0; x < mapSize; ++x) {
                for (int y = 0; y < mapSize; ++y) {
                    recycleCount++;
                    if (grid[x][y] != null) {
                        grid[x][y].releaseDrawable();
                    }
                }
            }
        }
    }

    public void clearGrid() {
        if (grid != null /*&& recycleCount >= 249999*/) {
            Log.i("grid null", "GRID NULL");
            recycleCount = 0;
            grid = null;
        }
    }
}
