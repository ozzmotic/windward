package edu.neu.madcourse.windward;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class WorldObject {

    private Drawable d;
    private int type;
    private int column;
    private int row;
    private int width;
    public int opacity = 0;
    private float degrees = 0;

    public int waveStage = 0;

    public WorldObject(int w, int type) {
        setType(type);

        width = w;
    }

    public WorldObject() {

    }

    public Boolean place = false;

    //Render bitmap at current location
    public void doDraw(Canvas canvas) {

        if (type == 3) {

            //degrees += (0.1f * GameView.timeScaling) % 360;
            //opacity = (int) map((float) Math.sin(Math.toRadians(degrees)), -1.f, 1.f, 0.f, 255.f);


            if (place) {
               if (opacity < 255) {
                   opacity += 5;
               }
            }

            if (!place) {
                if (opacity > 0) {
                    opacity -= 5;
                }
            }


            d.setAlpha(opacity);
        }

        d.draw(canvas);

    }

    static public final float map(float value,
                                  float start1, float stop1,
                                  float start2, float stop2) {
        return start2 + (stop2 - start2) * ((value - start1) / (stop1 - start1));
    }

    //Setters
    public void setBounds(int x, int y) {
        row = x;
        column = y;

        if (d != null) {
            d.setBounds(column, row, column + width, row + width);
        }
    }

    public void setType(int t) {
        type = t;

        switch (t) {
            case -1:
                if (d != null) {
                    //d = null;
                }
                d = GameScreen.getMyContext().getResources().getDrawable(R.drawable.test);
                break;
            case 0:
                if (d != null) {
                    //d = null;
                }
                d = GameScreen.getMyContext().getResources().getDrawable(R.drawable.ship);
                break;
            case 1:
                if (d != null) {
                    //d = null;
                }
                d = GameScreen.getMyContext().getResources().getDrawable(R.drawable.island);
                break;
            case 2:
                if (d != null) {
                    //d = null;
                }
                d = GameScreen.getMyContext().getResources().getDrawable(R.drawable.bottle);
                break;
            case 3:
                if (d != null) {
                    //d = null;
                }
                d = GameScreen.getMyContext().getResources().getDrawable(R.drawable.wave2);
                break;
            case 4:
                if (d != null) {
                    //d = null;
                }
                d = GameScreen.getMyContext().getResources().getDrawable(R.drawable.whale);
        }
    }

    public void releaseDrawable() {
        d = null;
    }

    public void rotate(int orientation) {
        if (type != 0) {
            Log.w("WorldObject", "Tried to rotate a non-ship");
            return;
        }

        switch (orientation) { //0 = up, 1 = right, 2 = down, 3 = left
            case 0:
                if (d != null) {
                    //d = null;
                }
                d = GameScreen.getMyContext().getResources().getDrawable(R.drawable.ship);
                break;
            case 1:
                if (d != null) {
                    //d = null;
                }
                d = GameScreen.getMyContext().getResources().getDrawable(R.drawable.ship_right);
                break;
            case 2:
                if (d != null) {
                    //d = null;
                }
                d = GameScreen.getMyContext().getResources().getDrawable(R.drawable.ship_down);
                break;
            case 3:
                if (d != null) {
                    //d = null;
                }
                d = GameScreen.getMyContext().getResources().getDrawable(R.drawable.ship_left);
                break;
        }

        d.setBounds(column, row, column + width, row + width);

    }

    //Getters
    public int getType() {
        return type;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    //Serialization
    public String serialize() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(WorldObject.class, new WorldObjectDeserializer());
        gsonBuilder.registerTypeAdapter(WorldObject.class, new WorldObjectSerializer());
        Gson gson = gsonBuilder.create();

        return gson.toJson(this);
    }

    public static WorldObject create(String serializedData) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(WorldObject.class, new WorldObjectDeserializer());
        gsonBuilder.registerTypeAdapter(WorldObject.class, new WorldObjectSerializer());
        Gson gson = gsonBuilder.create();

        return gson.fromJson(serializedData, WorldObject.class);
    }

    private static class WorldObjectSerializer implements JsonSerializer<WorldObject> {
        public JsonElement serialize(WorldObject t, Type type, JsonSerializationContext jsc) {
            JsonObject jo = new JsonObject();
            jo.addProperty("type", t.getType());
            jo.addProperty("column", t.getColumn());
            jo.addProperty("row", t.getRow());
            return jo;
        }
    }

    private static class WorldObjectDeserializer implements JsonDeserializer<WorldObject> {
        public WorldObject deserialize(JsonElement je, Type type, JsonDeserializationContext jdc) throws JsonParseException {
            JsonObject jo = je.getAsJsonObject();
            WorldObject t = new WorldObject();
            t.setType(jo.getAsJsonPrimitive("type").getAsInt());
            t.setBounds(jo.getAsJsonPrimitive("column").getAsInt(),
                    jo.getAsJsonPrimitive("row").getAsInt());

            return t;
        }
    }
}
