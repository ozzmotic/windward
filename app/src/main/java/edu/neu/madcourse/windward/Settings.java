package edu.neu.madcourse.windward;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import static edu.neu.madcourse.windward.GameScreen.*;

public class Settings extends Activity implements SeekBar.OnSeekBarChangeListener {

    private SeekBar senseBar;
    private SharedPreferences prefs;
    boolean musicSetting;
    boolean soundEffectSetting;
    CheckBox musicCheckBox;
    CheckBox soundEffectCheckBox;

    private TextView exhaleInst;
    private BreathListener breathListener;
    private BreathDetectTask breathDetectorTask;

    public static boolean heardBreath = false;

    private final Handler myHandlerBreathTester = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.windward_settings);

        senseBar=(SeekBar)findViewById(R.id.windward_sensitivity_seekBar);
        senseBar.setOnSeekBarChangeListener(this);
        senseBar.setProgress(BreathListener.sensitivity);

        prefs = getSharedPreferences(GameScreen.class.getSimpleName(),
                Context.MODE_PRIVATE);
        musicSetting = prefs.getBoolean("MUSIC",true);
        soundEffectSetting = prefs.getBoolean("SOUND_EFFECTS",true);

        musicCheckBox = (CheckBox)findViewById(R.id.checkbox_music);
        soundEffectCheckBox = (CheckBox)findViewById(R.id.checkbox_soundeffects);

        musicCheckBox.setChecked(musicSetting);
        soundEffectCheckBox.setChecked(soundEffectSetting);

        exhaleInst = (TextView)findViewById(R.id.Instruction);
        exhaleInst.setText(R.string.exhale_calibration_inst);

        //Microphone
        if (AudioManager.hasMicrophone(this)) {
            breathListener = new BreathListener(2, 250, 1500, 200, 500);
            breathDetectorTask = new BreathDetectTask(this);
            breathDetectorTask.execute(breathListener);
            Log.d("do breathListener", "do breathing test in settings");
        }

        Timer toastTimer = new Timer();
        toastTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                breathTester();
            }
        }, 0, 200);
    }

    private void breathTester() {
        myHandlerBreathTester.post(myRunnable);
    }

    private long labelTimer = 0;
    final Runnable myRunnable = new Runnable() {
        public void run() {

            if (heardBreath) {
                exhaleInst.setText("Ship in Motion");
                exhaleInst.setTextSize(35.f);
                labelTimer = System.currentTimeMillis();
                heardBreath = false;
            }

            if (!heardBreath && exhaleInst.getText() == "Ship in Motion" && System.currentTimeMillis() - labelTimer > 2000) {
                exhaleInst.setText(R.string.exhale_calibration_inst);
                exhaleInst.setTextSize(15.f);
            }

        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        stopMic();
        breathDetectorTask = null;
        breathDetectorTask = new BreathDetectTask(this);
        breathDetectorTask.execute(breathListener);

    }

    @Override
    protected void onPause() {
        super.onPause();

        stopMic();
    }

    //Microphone methods
    private void stopMic() {
        shutDownTaskIfNecessary(breathDetectorTask);
    }

    private void shutDownTaskIfNecessary(final AsyncTask task) {
        if (task != null && !task.isCancelled()) {
            //if (task.getStatus().equals(AsyncTask.Status.RUNNING)
            //       || task.getStatus().equals(AsyncTask.Status.PENDING)) {
            task.cancel(true);

            Log.i("Task", "Task shutdown from settings");
        } else {
            Log.i("Task", "Task not running from settings");
        }

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

        if (exhaleInst != null) {
            if (exhaleInst.getText() == "Ship in Motion") {
                exhaleInst.setText(R.string.exhale_calibration_inst);
                exhaleInst.setTextSize(15.f);
                Log.i("settings", "SENSITIVITY " + BreathListener.sensitivity);
            }
        }

        BreathListener.sensitivity = i;
        BreathListener.changedSensitivity = true;

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public void resetSensitivity(View v) {
        senseBar.setProgress(50);
    }

    public void toggleMusic(View v) {
        if (musicSetting) {
            musicSetting = false;
        } else {
            musicSetting = true;
        }

        musicCheckBox.setChecked(musicSetting);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("MUSIC",musicSetting);
        editor.apply();
    }

    public void toggleSoundEffects(View v) {
        if (soundEffectSetting) {
            soundEffectSetting = false;
        } else {
            soundEffectSetting = true;
        }

        soundEffectCheckBox.setChecked(soundEffectSetting);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("SOUND_EFFECTS",soundEffectSetting);
        editor.apply();
    }

    public void viewAcknowledgments(View v) {
        Intent intent = new Intent(this, Acknowledgments.class);
        startActivity(intent);
    }
}
