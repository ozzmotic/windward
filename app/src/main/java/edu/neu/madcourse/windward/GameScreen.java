package edu.neu.madcourse.windward;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class GameScreen extends FragmentActivity implements View.OnClickListener {

    public static GameView gameView;
    public static Context context;
    public static boolean imageOnce = false;
    private static String TAG = "GameScreen";
    public static boolean closedIslandWindow = false;
    public static boolean createNewBreathTask = false;
    public Typeface distFont;

    private Typeface windwardFont;
    private BreathListener breathListener;
    private BreathDetectTask breathDetectorTask;
    private LayoutInflater layoutInflater;
    private boolean hasMic = false;
    private boolean musicEnabled;
    private boolean soundEffectsEnabled;

    private final Handler ToastHandler = new Handler();
    private final Handler myHandlerInstagram = new Handler();
    //private static boolean showExhaleToast = true;

    private long prevWaveTime = 0;
    private long prevWaveTime2 = 0;

    private boolean doLoadingImage = true;
    private boolean doCheckInternet = true;

    public static int rangeThreshold = 0;
    public static int volThreshold = 0;
    public static int freqThreshold = 0;

    public static boolean runOnceThisSession = false;


    private PopupWindow introWindow = null;
    private PopupWindow popupAlreadyVisitWindow = null;
    private PopupWindow imageWindow = null;
    private PopupWindow questWindow = null;
    private PopupWindow investigateWindow = null;
    private PopupWindow bottleWindow = null;

    private Button quitButton;
    private Button leftButton;
    private Button rightButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        runOnceThisSession = true;

        super.onCreate(savedInstanceState);
        boolean newGame = getIntent().getBooleanExtra("NEW", true);

        //Microphone
        if (AudioManager.hasMicrophone(this)) {
            hasMic = true;

            breathListener = new BreathListener(2, 250, 1500, 200, 500, this);
            //breathListener.setDevice(0);

            breathDetectorTask = new BreathDetectTask(this);
            breathDetectorTask.execute(breathListener);
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        context = getApplicationContext();

        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        layoutInflater =
                (LayoutInflater) getBaseContext().getSystemService
                        (LAYOUT_INFLATER_SERVICE);

        Display display = wm.getDefaultDisplay();
        Point size = getDisplaySize(display);
        int mWidth = size.x;
        int mHeight = size.y;

        FrameLayout game = new FrameLayout(this);

        if (gameView != null) {
            Log.d("CLEAN UP GAME VIEW", "CLEAN UP GAME VIEW");

            gameView.stopDraw = true;
            gameView.recycleBitmap();
            //gameView.recycleDrawables();
            gameView.clearGrid();
        }

        gameView = new GameView(this, mWidth, mHeight);
        distFont = Typeface.createFromAsset(getAssets(), "fonts/windlass.ttf");
        gameView.setDistFont(distFont);

        SharedPreferences prefs = getPreferences(context);
        if (!newGame) {

            //ship on whale error
            //480, 455 whale loc
            if (prefs.getInt("SHIP_ROW", 498) == 480 && prefs.getInt("SHIP_COL", 9) == 455) {
                gameView.setShipRow(482);
                gameView.setShipCol(455);
            } else {
                gameView.setShipRow(prefs.getInt("SHIP_ROW", 498));
                gameView.setShipCol(prefs.getInt("SHIP_COL", 9));
            }

            gameView.bottlesCollected = prefs.getInt("BOTTLES_COLLECTED", 0);
            gameView.currentQuest = prefs.getInt("CURRENT_QUEST", 0);
            gameView.setShipOrientation(prefs.getInt("SHIP_ORIENTATION", 0));

            for (int n = 0; n < 12; n++) {
                gameView.setGotBottles(prefs.getBoolean("BOTTLE_" + n, false), n);
            }

            // make compass show up on continue game
            gameView.prepCompass();

            gameView.doneWithIntroDialog = true;
            gameView.showIntroDialog = false;
        } else {
            gameView.doneWithIntroDialog = false;
            gameView.showIntroDialog = true;
        }

        // QQQ
        gameView.placeWaves();
        gameView.placeShip();
        // rotate ship to saved orientation
        gameView.shipCoastingReOrientation();

        if (gameView.currentQuest > 6) {
            gameView.startExplorationMode();
        } else {
            gameView.placeIslands();
        }

        gameView.placeBottles();
        gameView.updateWindow();
        gameView.setAllBoundsInWindow();

        musicEnabled = prefs.getBoolean("MUSIC", true);
        soundEffectsEnabled = prefs.getBoolean("SOUND_EFFECTS", true);
        windwardFont = Typeface.createFromAsset(getAssets(), "fonts/windlass.ttf");

        LinearLayout gameWidgets = new LinearLayout(this);

         quitButton = new Button(this);
         leftButton = new Button(this);
         rightButton = new Button(this);

        LinearLayout.LayoutParams quitButtonParams = new LinearLayout.LayoutParams(pxToDp(105),
                pxToDp(50));
        quitButtonParams.setMargins(5, mHeight - pxToDp(70), 5, 5);
        quitButton.setLayoutParams(quitButtonParams);
        quitButton.setText("Menu");
        quitButton.setTextSize(12);
        quitButton.setTypeface(windwardFont);
        quitButton.setId(R.id.game_quit);
        quitButton.setBackgroundResource(R.drawable.custombutton);
        quitButton.setOnClickListener(this);

        LinearLayout.LayoutParams leftButtonParams = new LinearLayout.LayoutParams(pxToDp(130),
                pxToDp(50));
        leftButtonParams.setMargins(5, mHeight - pxToDp(70), 5, 5);
        leftButton.setLayoutParams(quitButtonParams);
        leftButton.setText("Steer Left");
        leftButton.setTextSize(12);
        leftButton.setTypeface(windwardFont);
        leftButton.setId(R.id.game_steer_left);
        leftButton.setBackgroundResource(R.drawable.custombutton);
        leftButton.setOnClickListener(this);

        LinearLayout.LayoutParams rightButtonParams = new LinearLayout.LayoutParams(pxToDp(130),
                pxToDp(50));
        rightButtonParams.setMargins(5, mHeight - pxToDp(70), 5, 5);
        rightButton.setLayoutParams(quitButtonParams);
        rightButton.setText("Steer Right");
        rightButton.setTextSize(12);
        rightButton.setTypeface(windwardFont);
        rightButton.setId(R.id.game_steer_right);
        rightButton.setBackgroundResource(R.drawable.custombutton);
        rightButton.setOnClickListener(this);

        gameWidgets.addView(leftButton);
        gameWidgets.addView(quitButton);
        gameWidgets.addView(rightButton);

        game.addView(gameView);
        game.addView(gameWidgets);

        setContentView(game);

        Timer toastTimer = new Timer();
        toastTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                UpdateToast();
            }
        }, 0, 200);

        // Island image loading from instagram
        IslandImage.setSearchTags();
        Timer imageTimer = new Timer();
        imageTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runInstagramTimer();
            }
        }, 0, 100);

        prevWaveTime = System.currentTimeMillis();
        prevWaveTime2 = prevWaveTime;
    }

    private void UpdateToast() {
        ToastHandler.post(myRunnable);
    }

    private void runInstagramTimer() {
        myHandlerInstagram.post(runnableIslandImageLoading);
    }

    public void showAlertDialog(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle(title).setMessage(message);

        // set dialog message
        if (title.equals("")) {
            alertDialogBuilder.setCancelable(false)
                    .setPositiveButton("I get it", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            BreathListener.doneWithSettings = true;
                            dialog.dismiss();
                        }
                    });

        } else {
            alertDialogBuilder.setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            BreathListener.readyToCalibrate = true;
                            dialog.dismiss();
                        }
                    });
        }

        // create alert dialog
        AlertDialog mAlertDialog = alertDialogBuilder.create();

        // show it
        mAlertDialog.show();
    }

    private boolean waveStageSwitch = false;
    private boolean placeWavesAgain = false;
    private boolean resumedState = true;
    private long prevEndWorldToastTime = -3500;
    public static boolean didPausedState = false;

    public boolean doPopupWindowOnce = true;
    private boolean doBreathRestToast = false;
    private long prevBreathRestToastTime = -3500;

    final Runnable myRunnable = new Runnable() {
        public void run() {

            // end of world
            if (gameView.showEndWorldToast) {

                if (System.currentTimeMillis() - prevEndWorldToastTime > 3500) {
                    Toast endWorldToast = Toast.makeText(GameScreen.context, "Let's not enter dangerous waters. Turn around and explore the rest of the world.",
                            Toast.LENGTH_LONG);
                    endWorldToast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                    endWorldToast.show();
                    prevEndWorldToastTime = System.currentTimeMillis();
                }

                gameView.showEndWorldToast = false;
            }

            if (resumedState) {

                // QQQ
                if (gameView.stopDraw == false) {
                    if (System.currentTimeMillis() - prevWaveTime > 3000) {
                        gameView.clearWaves();

                        placeWavesAgain = true;

                        //Log.i("place waves", "clear waves");
                        prevWaveTime += 3000;
                    } else {
                        if (placeWavesAgain) {
                            gameView.placePlayerWaves();
                            gameView.updateWindow();
                            gameView.setAllBoundsInWindow();
                            placeWavesAgain = false;
                        }
                    }
                }

                if (BreathListener.showReadyToCalibrateDialog) {
                    //show dialog asking user ready to exhale
                    showAlertDialog("Ready to sail?", "Play with headphones in a quiet place. \n\nBreath in deeply. " +
                            "Exhale into the phone gently after you press \"yes.\"" + "\n\nIf the boat does not move easily" +
                            " when you exhale, click \"Menu\" then \"Help\"");
                    BreathListener.showReadyToCalibrateDialog = false;
                }

                if (gameView.gettingCloserToTarget) {

                    Toast closer = Toast.makeText(GameScreen.context, "Getting close to your destination.",
                            Toast.LENGTH_LONG);
                    closer.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                    closer.show();

                    gameView.gettingCloserToTarget = false;
                }

                if (gameView.firstTimeDistanceToast) {

                    Toast closer = Toast.makeText(GameScreen.context, "You're traveling closer to your destination.",
                            Toast.LENGTH_LONG);
                    closer.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                    closer.show();

                    gameView.firstTimeDistanceToast = false;
                }

                if (gameView.gettingFarFromTarget) {

                    Toast closer = Toast.makeText(GameScreen.context, "Steer the boat. You are moving away from your destination.",
                            Toast.LENGTH_LONG);
                    closer.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                    closer.show();

                    gameView.gettingFarFromTarget = false;
                }

                if (BreathListener.showHowUseSettings) {
                    /* QQQ
                    //showAlertDialog("", "If the boat does not respond " +
                    //        "correctly to your exhale, click the \"Help\" button in the main menu");
                    Toast settingsToast = Toast.makeText(GameScreen.context, "If the boat does not move easily" +
                                    " when you exhale, click \"Menu\" then \"Help\"",
                            Toast.LENGTH_LONG);
                    settingsToast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                    settingsToast.show();

                    Toast settingsToast2 = Toast.makeText(GameScreen.context, "If the boat does not move easily" +
                                    " when you exhale, click \"Menu\" then \"Help\"",
                            Toast.LENGTH_LONG);
                    settingsToast2.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                    settingsToast2.show();

                    BreathListener.showHowUseSettings = false;*/
                }


                if (BreathListener.doTutorialToast_firstMove) {

                    Toast firstMoveToast = Toast.makeText(GameScreen.context, "Great, you are the wind that moves the boat",
                            Toast.LENGTH_LONG);
                    firstMoveToast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                    firstMoveToast.show();

                    //displayCompassHelpText();

                    BreathListener.doTutorialToast_firstMove = false;
                }

                if (BreathListener.doHyperVentilationFeedback) {

                    if (quitButton.isEnabled()) {
                        Toast hyperVentToast = Toast.makeText(GameScreen.context, "Relax, you will hyperventilate if you exhale too frequently",
                                Toast.LENGTH_LONG);
                        hyperVentToast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                        hyperVentToast.show();
                    } else {
                        Toast hyperVentToast = Toast.makeText(GameScreen.context, "Stop exhaling and click on investigate or read the message.",
                                Toast.LENGTH_LONG);
                        hyperVentToast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        hyperVentToast.show();
                    }

                    BreathListener.doHyperVentilationFeedback = false;
                }

                if (BreathListener.doShallowExhaleFeedback) {

                    if (quitButton.isEnabled()) {
                        Toast shallowToast = Toast.makeText(GameScreen.context, "Breathe and exhale deeply. Longer exhales will help propel the ship",
                                Toast.LENGTH_LONG);
                        shallowToast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                        shallowToast.show();
                    } else {
                        Toast shallowToast = Toast.makeText(GameScreen.context, "Stop exhaling and click on investigate or read the message.",
                                Toast.LENGTH_LONG);
                        shallowToast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                        shallowToast.show();
                    }

                    BreathListener.doShallowExhaleFeedback = false;
                }

                if ( (gameView.canInteract && doPopupWindowOnce) || investigateWindowDismiss) {
                    if (soundEffectsEnabled) {
                        Music.playSoundEffect(context, R.raw.windward_collect);
                    }
                    gameView.canInteract = false;

                    // QQQ
                    //investigateWindowDismiss = false;
                    //investigateWindowUserDismiss = false;

                    doPopupWindowOnce = false;
                    final int obj = gameView.temp;
                    quitButton.setEnabled(false);
                    leftButton.setEnabled(false);
                    rightButton.setEnabled(false);

                    View popupView = layoutInflater.inflate(R.layout.popup, null);
                    investigateWindow = new PopupWindow(popupView,
                            ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

                    final TextView textView = (TextView)popupView.findViewById(R.id.found);
                    textView.setTypeface(windwardFont);
                    final Button investButton = (Button) popupView.findViewById(R.id.investigate);
                    investButton.setTypeface(windwardFont);
                    investButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            gameView.interactWithWorldObject(obj);
                            investigateWindow.dismiss();
                            investigateWindowUserDismiss = true;
                            quitButton.setEnabled(true);
                            leftButton.setEnabled(true);
                            rightButton.setEnabled(true);

                        }
                    });

                    // QQQ causes error on exit of game
                    investigateWindow.showAtLocation(gameView, Gravity.CENTER_HORIZONTAL | Gravity.TOP, 0, 50);
                }

                if (gameView.displayBottleMessage) {
                    gameView.displayBottleMessage = false;
                    quitButton.setEnabled(false);
                    leftButton.setEnabled(false);
                    rightButton.setEnabled(false);

                    View popupView = layoutInflater.inflate(R.layout.bottle_message, null);
                    final TextView bottleText = (TextView) popupView.findViewById(R.id.bottle_message_text);

                    switch (gameView.bottlesCollected) {
                        case 0:
                            bottleText.setText(R.string.bottle_1);
                            break;
                        case 1:
                            bottleText.setText(R.string.bottle_2);
                            break;
                        case 2:
                            bottleText.setText(R.string.bottle_3);
                            break;
                        case 3:
                            bottleText.setText(R.string.bottle_4);
                            break;
                        case 4:
                            bottleText.setText(R.string.bottle_5);
                            break;
                        case 5:
                            bottleText.setText(R.string.bottle_6);
                            break;
                        case 6:
                            bottleText.setText(R.string.bottle_7);
                            break;
                        case 7:
                            bottleText.setText(R.string.bottle_8);
                            break;
                        case 8:
                            bottleText.setText(R.string.bottle_9);
                            break;
                        case 9:
                            bottleText.setText(R.string.bottle_10);
                            break;
                        case 10:
                            bottleText.setText(R.string.bottle_11);
                            break;
                        case 11:
                            bottleText.setText(R.string.bottle_12);
                            break;
                    }
                    bottleWindow = new PopupWindow(popupView,
                            ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

                    final Button bottleButton = (Button) popupView.findViewById(R.id.dismiss_bottle_message);
                    bottleButton.setTypeface(windwardFont);
                    bottleButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            gameView.bottlesCollected++;
                            bottleWindow.dismiss();
                            doPopupWindowOnce = true;
                            quitButton.setEnabled(true);
                            leftButton.setEnabled(true);
                            rightButton.setEnabled(true);
                        }
                    });
                    bottleWindow.showAtLocation(gameView, Gravity.CENTER_HORIZONTAL | Gravity.TOP, 0, 50);
                }

                if (gameView.displayQuestMessage || questWindowDismiss) {
                    gameView.displayQuestMessage = false;
                    quitButton.setEnabled(false);
                    leftButton.setEnabled(false);
                    rightButton.setEnabled(false);
                    //QQQ
                    //questWindowDismiss = false;
                    //questWindowUserDismiss = false;

                    View popupView = layoutInflater.inflate(R.layout.quest_message, null);
                    final TextView questText = (TextView) popupView.findViewById(R.id.quest_message_text);
                    switch (gameView.currentQuest) {
                        case 0:
                            questText.setText(R.string.quest_1);
                            break;
                        case 1:
                            questText.setText(R.string.quest_2a);
                            break;
                        case 3:
                            questText.setText(R.string.quest_3a);
                            break;
                        case 5:
                            questText.setText(R.string.quest_4);
                            break;
                        case 6:
                            questText.setText(R.string.quest_5);
                            break;
                    }
                    questWindow = new PopupWindow(popupView,
                            ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

                    final Button questButton = (Button) popupView.findViewById(R.id.dismiss_quest_message);
                    questButton.setTypeface(windwardFont);
                    questButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (gameView.currentQuest == 1) {
                                questText.setText(R.string.quest_2b);
                            } else if (gameView.currentQuest == 3) {
                                questText.setText(R.string.quest_3b);
                            } else {
                                gameView.displayQuestMessage = false;
                                questWindow.dismiss();
                                doPopupWindowOnce = true;
                                // QQQ
                                //questWindowUserDismiss = true;
                                quitButton.setEnabled(true);
                                leftButton.setEnabled(true);
                                rightButton.setEnabled(true);

                            }
                            gameView.currentQuest++;

                            if (gameView.currentQuest == 1) {
                                displayCompassHelpText();
                                displayCompassHelpText();
                                displayCompassHelpText();
                            }
                            if (gameView.currentQuest == 7) {
                                gameView.startExplorationMode();
                            } else {
                                gameView.prepCompass();
                                gameView.preparedCompass = true;
                            }
                        }
                    });
                    questWindow.showAtLocation(gameView, Gravity.CENTER_HORIZONTAL | Gravity.TOP, 0, 50);
                }

                if (GameView.displayIslandImage) {

                    if (!imageOnce) {
                        View popupView = layoutInflater.inflate(R.layout.island_image, null);
                        final ImageView islandImage = (ImageView) popupView.findViewById(R.id.island_image);
                        islandImage.setImageDrawable(new BitmapDrawable(getResources(), IslandImage.loadedImage));
                        Log.i(TAG, "loadedImage set");

                        imageWindow = new PopupWindow(popupView,
                                ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
                        quitButton.setEnabled(false);
                        leftButton.setEnabled(false);
                        rightButton.setEnabled(false);

                        final Button dismissButton = (Button) popupView.findViewById(R.id.dismiss_island_image);
                        dismissButton.setTypeface(windwardFont);
                        dismissButton.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                GameView.displayIslandImage = false;
                                IslandImage.enableBreathAsyncTask = true;
                                doLoadingImage = true;
                                doCheckInternet = true;
                                Log.d("onclick from display island image", "???");
                                imageWindow.dismiss();
                                doPopupWindowOnce = true;
                                quitButton.setEnabled(true);
                                leftButton.setEnabled(true);
                                rightButton.setEnabled(true);
                                // tell user to take a break before exhaling again
                                doBreathRestToast = true;
                                closedIslandWindow = true;
                                //gameView.currentQuest++;
                            }

                        });
                        imageWindow.showAtLocation(gameView, Gravity.CENTER_HORIZONTAL | Gravity.TOP, 0, 50);
                        imageOnce = true;
                    }
                    GameView.displayIslandImage = false;

                }

                if (doBreathRestToast) {
                    if (System.currentTimeMillis() - prevBreathRestToastTime > 3500) {
                        Toast endWorldToast = Toast.makeText(GameScreen.context, "You've come far. Please take a break now before exploring again.",
                                Toast.LENGTH_LONG);
                        endWorldToast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                        endWorldToast.show();
                        prevBreathRestToastTime = System.currentTimeMillis();
                    }
                }

                if (gameView.currentQuest >= 7) {
                    if (createNewBreathTask) {
                        doBreathRestToast = false;
                        Toast endWorldToast = Toast.makeText(GameScreen.context, "Ok, hope you're now rested and ready for more exploration.",
                                Toast.LENGTH_LONG);
                        endWorldToast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                        endWorldToast.show();
                        createNewBreathTask = false;
                    }
                }

                if (gameView.displayIntroDialog) {
                    gameView.displayIntroDialog = false;

                    View popupView = layoutInflater.inflate(R.layout.quest_message, null);
                    final TextView introText = (TextView) popupView.findViewById(R.id.quest_message_text);
                    introText.setText(R.string.intro_1);


                    /*final PopupWindow*/ introWindow = new PopupWindow(popupView,
                            ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

                    quitButton.setEnabled(false);
                    leftButton.setEnabled(false);
                    rightButton.setEnabled(false);

                    final Button questButton = (Button) popupView.findViewById(R.id.dismiss_quest_message);
                    questButton.setTypeface(windwardFont);
                    questButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (gameView.introTextCounter == 0) {
                                introText.setText(R.string.intro_2);
                            } else if (gameView.introTextCounter == 1) {
                                introText.setText(R.string.intro_3);
                            } else {
                                gameView.displayIntroDialog = false;
                                gameView.doneWithIntroDialog = true;
                                //gameView.prepCompass();
                                introWindow.dismiss();
                                quitButton.setEnabled(true);
                                leftButton.setEnabled(true);
                                rightButton.setEnabled(true);

                            }
                            gameView.introTextCounter++;
                        }
                    });

                    //if (introWindow != null)
                    introWindow.showAtLocation(gameView, Gravity.CENTER_HORIZONTAL | Gravity.TOP, 0, 50);
                }

                if (gameView.displayAlreadyVisitedDialog) {
                    gameView.displayAlreadyVisitedDialog = false;
                    quitButton.setEnabled(false);
                    leftButton.setEnabled(false);
                    rightButton.setEnabled(false);

                    View popupView = layoutInflater.inflate(R.layout.quest_message, null);
                    final TextView introText = (TextView) popupView.findViewById(R.id.quest_message_text);
                    introText.setText(R.string.already_visited);
                    popupAlreadyVisitWindow = new PopupWindow(popupView,
                            ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

                    final Button questButton = (Button) popupView.findViewById(R.id.dismiss_quest_message);
                    questButton.setTypeface(windwardFont);
                    questButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            gameView.displayAlreadyVisitedDialog = false;
                            popupAlreadyVisitWindow.dismiss();
                            doPopupWindowOnce = true;
                            quitButton.setEnabled(true);
                            leftButton.setEnabled(true);
                            rightButton.setEnabled(true);
                        }
                    });

                    popupAlreadyVisitWindow.showAtLocation(gameView, Gravity.CENTER_HORIZONTAL | Gravity.TOP, 0, 50);
                }
            } //resumed state
        }
    };


    final Runnable runnableIslandImageLoading = new Runnable() {
        public void run() {

            if (resumedState) {
                if (PausedScreen.doQuitFromPausedScreen) {
                    // close reminding pop up windows to prevent error
                    if (introWindow != null) {
                        introWindow.dismiss();
                    }
                    if (popupAlreadyVisitWindow != null) {
                        popupAlreadyVisitWindow.dismiss();
                    }
                    if (imageWindow != null) {
                        imageWindow.dismiss();
                    }
                    if (questWindow != null) {
                        questWindow.dismiss();
                    }
                    if (investigateWindow != null) {
                        investigateWindow.dismiss();
                    }
                    if (bottleWindow != null) {
                        bottleWindow.dismiss();
                    }

                    gameView.stopDraw = true;
                    gameView.recycleBitmap();
                    //gameView.recycleDrawables();
                    gameView.clearGrid();
                    finish();
                }
            }


            if (resumedState) {
                if (IslandImage.getInstagramImage) {
                    //Log.i( "???", BreathDetectTask.numberCreated + " " );
                    IslandImage.enableBreathAsyncTask = false;
                    stopMic();
                    IslandImage.doInstagram(context);

                    if (doLoadingImage) {
                        Toast.makeText(context, "loading image...", Toast.LENGTH_SHORT).show();
                        doLoadingImage = false;
                    }

                    // QQQ
                    if (!didPausedState) {
                        IslandImage.getInstagramImage = false;
                    }
                }

                if (IslandImage.doCheckInternetConnection && doCheckInternet) {
                    Toast.makeText(context, "check internet connection", Toast.LENGTH_SHORT).show();
                    IslandImage.doCheckInternetConnection = false;
                    doCheckInternet = false;
                }

            /*
            if (IslandImage.hasImage) {
                Toast.makeText(context, IslandImage.instagramImageUrl, Toast.LENGTH_LONG).show();
                IslandImage.hasImage = false;
            }*/

                if (IslandImage.enableBreathAsyncTask) {
                    stopMic();
                    breathDetectorTask = new BreathDetectTask(context);
                    breathDetectorTask.execute(breathListener);
                    IslandImage.enableBreathAsyncTask = false;
                }
            } //resumed state
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.game_quit:
                PausedScreen.doQuitFromPausedScreen = false;
                Music.stop(this);
                Intent intent = new Intent(this, PausedScreen.class);
                startActivity(intent);
                break;
            case R.id.game_steer_left:
                gameView.steerShipLeft();

                //IslandImage.getInstagramImage = true;
                //Log.i("Instagram test", "click test");

                break;
            case R.id.game_steer_right:
                gameView.steerShipRight();
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        //IslandImage.enableBreathAsyncTask = true;

        if (musicEnabled) {
            Music.play(context, R.raw.ocean_bg);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        gameView.stopDraw = false;
        resumedState = true;

        // allow movement after resume
        doPopupWindowOnce = true;

        //GameScreen.gameView.doneWithIntroDialog = true;

        if (hasMic) {
            IslandImage.enableBreathAsyncTask = false;
            stopMic();
            breathDetectorTask = null;
            //IslandImage.enableBreathAsyncTask = true;
            //breathListener = new BreathListener(2, 250, 1500, 200, 500, this);
            breathDetectorTask = new BreathDetectTask(this);
            breathDetectorTask.execute(breathListener);
        }

    }

    private boolean investigateWindowDismiss = false;
    private boolean investigateWindowUserDismiss = false;

    private boolean questWindowDismiss = false;
    private boolean questWindowUserDismiss = false;
    @Override
    protected void onPause() {
        super.onPause();

        Log.d("GAMESCREEN ON PAUSE", "GAME SCREEN ON PAUSE");

        // close reminding pop up windows to prevent error
        if (introWindow != null) {
            introWindow.dismiss();
        }
        if (popupAlreadyVisitWindow != null) {
            popupAlreadyVisitWindow.dismiss();
        }
        if (imageWindow != null) {
            imageWindow.dismiss();
        }
        if (questWindow != null) {
            questWindow.dismiss();
            /*
            if (!questWindowUserDismiss) {
                questWindowDismiss = true;
            }*/
        }
        if (investigateWindow != null) {
            investigateWindow.dismiss();
            /*
            if (!investigateWindowUserDismiss) {
                investigateWindowDismiss = true;
            }*/
        }
        if (bottleWindow != null) {
            bottleWindow.dismiss();
        }

        // instagram fetching calls
        didPausedState = true;
        // flag for handling resume in runnables
        resumedState = false;

        if (hasMic) {
            IslandImage.enableBreathAsyncTask = false;
            stopMic();
        }

        Music.stop(context);
        Music.stopCollectEffect(context);

        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("FIRST_RUN", false);
        editor.putInt("SHIP_ROW", gameView.getShipRow());
        editor.putInt("SHIP_COL", gameView.getShipCol());
        editor.putInt("CURRENT_QUEST", gameView.currentQuest);
        editor.putInt("BOTTLES_COLLECTED", gameView.bottlesCollected);
        editor.putInt("SHIP_ORIENTATION", gameView.getShipOrientation());

        boolean[] bottles = gameView.getGotBottles();
        for (int n = 0; n < 12; n++) {
            editor.putBoolean("BOTTLE_" + n, bottles[n]);
        }
        editor.apply();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("GAMESCREEN ON STOP", "GAMESCREEN ON STOP");
        gameView.stopDraw = true;

    }

    public void moveShip() {
        gameView.moveShip();
    }

    public void spinOutShip() {
        gameView.spinOutShip();
    }

    public void shipCoastingReOrientation() {
        gameView.shipCoastingReOrientation();
    }

    private static Point getDisplaySize(final Display display) {
        final Point point = new Point();
        if (Build.VERSION.SDK_INT > 12) {
            display.getSize(point);
        } else { // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        return point;
    }

    public static int pxToDp(int px) {
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, getMyContext().getResources().getDisplayMetrics()));
    }

    public static Context getMyContext() {
        return context;
    }

    //Microphone methods
    private void stopMic() {
        shutDownTaskIfNecessary(breathDetectorTask);
    }

    private void shutDownTaskIfNecessary(final AsyncTask task) {
        if (task != null && !task.isCancelled()) {
            //if (task.getStatus().equals(AsyncTask.Status.RUNNING)
            //       || task.getStatus().equals(AsyncTask.Status.PENDING)) {
            task.cancel(true);

            Log.i("Task", "Task shutdown");
        } else {
            Log.i("Task", "Task not running");
        }

    }

    public int getVolFromPref() {
        final SharedPreferences prefs = getPreferences(context);
        volThreshold = prefs.getInt("volThreshold", -1);
        return prefs.getInt("volThreshold", -1);
        //Log.i("pref vol", volumeThreshold + " ");
    }

    public int getFreqFromPref() {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getInt("freqThreshold", -1);
        //Log.i("pref freq", freqThreshold + " ");
    }

    public int getRangeFromPref() {
        final SharedPreferences prefs = getPreferences(context);
        rangeThreshold = prefs.getInt("rangeThreshold", -1);
        return prefs.getInt("rangeThreshold", -1);
        //Log.i("pref freq", rangeThreshold + " ");
    }

    public int getSensitivityFromPref() {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getInt("sensitivity", -1);
    }

    public void storeCalibrationData(int v, int f, int r) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        freqThreshold = f;
        volThreshold  = v;
        rangeThreshold = r;
        editor.putInt("volThreshold", v);
        editor.putInt("freqThreshold", f);
        editor.putInt("rangeThreshold", r);
        editor.apply();

        Log.i("storeData", v + " " + f + " " + r);
    }

    public void storeSensitivityData(int sensitivity) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("sensitivity", sensitivity);
        editor.apply();

        Log.i("storeData static pref", " " + sensitivity);
    }

    private SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(GameScreen.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    public static void displayCompassHelpText() {
        Toast toast = Toast.makeText(GameScreen.context, "The compass (top right of screen) points to your destination and shows remaining distance. Steer your ship using the buttons below.",
                Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
}