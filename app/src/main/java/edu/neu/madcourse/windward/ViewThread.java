package edu.neu.madcourse.windward;
import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class ViewThread extends Thread {

    private GameView mGameView;
    private SurfaceHolder mHolder;
    private boolean mRun = false;

    public ViewThread(GameView gameView) {
        mGameView = null;
        mGameView = gameView;
        mHolder = gameView.getHolder();
    }

    public void setRunning(boolean run) {
        mRun = run;
    }

    @Override
    public void run() {
        Canvas canvas = null;

        while (mRun) {
            canvas = mHolder.lockCanvas();

            if (canvas != null) {
                mGameView.doDraw(canvas);
                mHolder.unlockCanvasAndPost(canvas);
            }
        }
    }
}
