package edu.neu.madcourse.windward;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

public class IslandImage {

    public static String INSTAGRAM_API_URL = "https://api.instagram.com/v1";
    public static String CLIENT_ID = "def20410b5134f7d9b828668775aee4a";
    public static String TAG = "island";
    public static int imageInc = 0;
    public static String instagramImageUrl = "";
    public static Boolean doCheckInternetConnection = false;
    public static Boolean enableBreathAsyncTask = false;
    public static Bitmap loadedImage = null;
    public static Boolean getInstagramImage = false;
    public static Boolean hasImage = false;

    private static String[] searchTagArray = new String[8];
    public static int searchImageTag = 0;

    private static int NUM_ASYNC = 0;

    public static void setSearchTags() {
        searchTagArray[0] = "island";
        searchTagArray[1] = "aleutianislands";
        searchTagArray[2] = "atoll";
        searchTagArray[3] = "islandscenery";
        searchTagArray[4] = "whalewatching";
        searchTagArray[5] = "oceanscenery";
        searchTagArray[6] = "pacificocean";
    }

    public static void doInstagram(Context mContext) {

        loadedImage = null;
        hasImage = false;
        //enableBreathAsyncTask = false;
        GameScreen.imageOnce = false;
        GameView.displayIslandImage = false;

        Random rSearchTag = new Random();
        searchImageTag = rSearchTag.nextInt(7);
        Log.i("Search Tag #", searchTagArray[searchImageTag]);
        TAG = searchTagArray[searchImageTag];

        Log.d("INSTAGRAM", "enter Instagram");

        /* don't do on main thread
        String urlString = INSTAGRAM_API_URL + "/tags/" + TAG + "/media/recent?client_id=" + CLIENT_ID;
        try {
            URL url = new URL(urlString);
            InputStream inputStream = url.openConnection().getInputStream();
            String response = streamToString(inputStream);
            JSONObject jsonObject = (JSONObject) new JSONTokener(response).nextValue();
            JSONArray jsonArray = jsonObject.getJSONArray("data");

            JSONObject imageJsonObject = jsonArray.getJSONObject(0).getJSONObject("images").getJSONObject("standard_resolution");
            String imageUrlString = imageJsonObject.getString("url");

            instagramImageUrl = imageUrlString;
            Bitmap imageBitmap = getImageBitmap(imageUrlString);
            instagramImageView.setImageBitmap(imageBitmap);

        } catch (Exception ex) {

            Log.d("INSTAGRAM URL", ex.toString());
        }*/

        Log.d("NUM OF ASYNC", NUM_ASYNC + "");
        if (NUM_ASYNC < 1) {
            new AsyncTask<Void, Void, Bitmap>() {
                @Override
                protected Bitmap doInBackground(Void... params) {
                    //NUM_ASYNC++;
                    String urlString = INSTAGRAM_API_URL + "/tags/" + TAG + "/media/recent?client_id=" + CLIENT_ID;

                    Log.d("INSTAGRAM", "doInBackground");

                    try {

                        Log.d("INSTAGRAM", "try block");

                        URL url = new URL(urlString);
                        InputStream inputStream = url.openConnection().getInputStream();
                        String response = streamToString(inputStream);
                        JSONObject jsonObject = (JSONObject) new JSONTokener(response).nextValue();
                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        JSONObject imageJsonObject = jsonArray.getJSONObject(imageInc).getJSONObject("images").getJSONObject("thumbnail");
                        String imageUrlString = imageJsonObject.getString("url");
                        //full_name
                        // see instagram.com/developer
                        String username = jsonArray.getJSONObject(imageInc).getJSONObject("user").getString("username");

                        instagramImageUrl = imageUrlString;
                        Bitmap imageBitmap = getImageBitmap(imageUrlString);

                        int arrayLength = jsonArray.length();
                        imageInc = (imageInc + 1) % arrayLength;

                        hasImage = true;


                        Log.i("INSTAGRAM JSON", jsonArray.length() + "" + " username " + username);

                        return imageBitmap;

                    } catch (Exception ex) {

                        doCheckInternetConnection = true;
                        hasImage = false;
                        Log.d("INSTAGRAM URL", ex.toString() + " " + doCheckInternetConnection);

                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Bitmap result) {
                    // default image
                    //loadedImage = BitmapFactory.decodeResource(GameScreen.getMyContext().getResources(), R.drawable.default_island);

//                if (result != null) {
//                    hasImage = true;
//                }

                    if (hasImage) {
                        loadedImage = result;
                    } else {
                        loadedImage = BitmapFactory.decodeResource(GameScreen.getMyContext().getResources(), R.drawable.default_island);
                    }

                    if (loadedImage != null) {
                        GameView.displayIslandImage = true;
                    }
                    //instagramImageView.setImageBitmap(result);

                    //renable breath async task
                    //enableBreathAsyncTask = true;

                    this.cancel(true);
                    NUM_ASYNC = 0;


                    IslandImage.getInstagramImage = false;

                    Log.d("INSTAGRAM URL", "INSTAGRAM URL Post Execute: " + instagramImageUrl + " image num " + imageInc);
                }

            }.execute();
        }

    }

    /*
    http://stackoverflow.com/questions/3870638/how-to-use-setimageuri-on-android
     */
    private static Bitmap getImageBitmap(String url) {
        Bitmap bm = null;
        try {
            URL aURL = new URL(url);
            URLConnection conn = aURL.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);
            bis.close();
            is.close();
        } catch (IOException e) {
            Log.e(TAG, "Error getting bitmap", e);
        }
        return bm;
    }

    /*
    http://grishma102.blogspot.com/2014/01/instagram-api-integration-in-android.html
     */
    public static String streamToString(InputStream p_is) {
        try {
            BufferedReader m_br;
            StringBuffer m_outString = new StringBuffer();
            m_br = new BufferedReader(new InputStreamReader(p_is));
            String m_read = m_br.readLine();
            while (m_read != null) {
                m_outString.append(m_read);
                m_read = m_br.readLine();
            }
            return m_outString.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
