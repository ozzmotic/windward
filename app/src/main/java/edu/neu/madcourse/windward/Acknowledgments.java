package edu.neu.madcourse.windward;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class Acknowledgments extends Activity {

   protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);

       setContentView(R.layout.acknowledgments);

       String[] ack = new String[6];
       ack[0] = getString(R.string.ack_instagram);
       ack[1] = getString(R.string.ack_instagram_a);
       ack[2] = getString(R.string.ack_instagram_b);
       ack[3] = getString(R.string.ack_book);
       ack[4] = getString(R.string.ack_sound);
       ack[5] = getString(R.string.phone_tested);

       TextView textView = (TextView)findViewById(R.id.ack_text);
       textView.setText(BulletListBuilder.getBulletList(" ",ack));

   }
}
