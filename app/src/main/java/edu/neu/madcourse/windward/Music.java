package edu.neu.madcourse.windward;

import android.content.Context;
import android.media.MediaPlayer;

public class Music {
    private static MediaPlayer mp = null;
    private static MediaPlayer mp_collectEffect = null;

    /** Stop old song and start new one */
    public static void play(Context context, int resource) {
        stop(context);

        // Start music only if not disabled in preferences
        //if (Prefs.getMusic(context)) {
            mp = MediaPlayer.create(context, resource);
            mp.setLooping(true);
            mp.start();
        //}
    }

    public static void playSoundEffect(Context context, int resource) {
        mp_collectEffect = MediaPlayer.create(context, resource);
        mp_collectEffect.setLooping(false);
        mp_collectEffect.start();
    }

    /** Stop the music */
    public static void stop(Context context) {
        if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }
    }

    public static void stopCollectEffect(Context context) {
        if (mp_collectEffect != null) {
            mp_collectEffect.stop();
            mp_collectEffect.release();
            mp_collectEffect = null;
        }
    }


}
