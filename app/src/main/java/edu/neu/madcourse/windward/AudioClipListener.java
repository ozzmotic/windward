package edu.neu.madcourse.windward;

public interface AudioClipListener
{
    public boolean heard(short [] audioData, int sampleRate);
}
