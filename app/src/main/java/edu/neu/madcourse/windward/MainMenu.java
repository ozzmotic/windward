package edu.neu.madcourse.windward;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainMenu extends Activity {

    public static boolean notFirstRun = false;
    private Typeface windwardFont;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_menu);

        windwardFont = Typeface.createFromAsset(getAssets(), "fonts/windlass.ttf");
        Button continueButton = (Button)findViewById(R.id.button_continue);
        continueButton.setTypeface(windwardFont);
        Button startButton = (Button)findViewById(R.id.button_start);
        startButton.setTypeface(windwardFont);
        Button quitButton = (Button)findViewById(R.id.button_quit);
        quitButton.setTypeface(windwardFont);
        Button settingsButton = (Button)findViewById(R.id.button_settings);
        settingsButton.setTypeface(windwardFont);
    }

    @Override
    protected void onStart() {
        super.onStart();

        SharedPreferences prefs = getSharedPreferences(GameScreen.class.getSimpleName(),
                Context.MODE_PRIVATE);
        if (prefs.getBoolean("MUSIC",true)) { Music.play(this, R.raw.windward_main_menu); }
    }

    public static boolean isFirstRunTime = false;
    @Override
    protected void onResume() {
        super.onResume();

        Button continueButton = (Button)findViewById(R.id.button_continue);
        Button settingsButton = (Button)findViewById(R.id.button_settings);

        if (isFirstRun()) {
            continueButton.setVisibility(View.INVISIBLE);
            isFirstRunTime = true;
        } else {
            continueButton.setVisibility(View.VISIBLE);
            notFirstRun = true;
        }

        if (!GameScreen.runOnceThisSession /*&& isFirstRun()*/) {
            settingsButton.setVisibility(View.INVISIBLE);
        } else {
            settingsButton.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();

        Music.stop(this);
    }

    public void startGame(View v) {
        // for testing
        // do calibration again when we restart the level
        // BreathListener.doCalibration();

        if (!isFirstRun()) {
            showNewGameWarning("New Game","Starting a new game will erase all progress from"
                + " your previous game." + "\n\nPlease play using headphones.");
        } else {
            newGame();
        }
    }

    public void newGame() {
        //finish();
        //System.gc();
        PausedScreen.doQuitFromPausedScreen = false;
        Intent intent = new Intent(this,GameScreen.class);
        intent.putExtra("NEW",true);
        startActivity(intent);
    }

    public void continueGame(View v) {
        if (!isFirstRun()) {
            //System.gc();
            //finish();
            PausedScreen.doQuitFromPausedScreen = false;
            Intent intent = new Intent(this,GameScreen.class);
            intent.putExtra("NEW",false);
            startActivity(intent);
        }
    }

    public void quitGame(View v) {
        finish();
        System.gc();
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }

    public void startSettings(View v) {
        if (GameScreen.runOnceThisSession /* !isFirstRun() */) {
            //System.gc();
            //finish();
            Intent intent = new Intent(this, Settings.class);
            startActivity(intent);
        }
    }

    //Warning/info dialog
    public void showNewGameWarning(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle(title).setMessage(message);

        // set dialog message
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Play game", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        newGame();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Go back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        // create alert dialog
        AlertDialog mAlertDialog = alertDialogBuilder.create();

        // show it
        mAlertDialog.show();
    }

    private boolean isFirstRun() {

        SharedPreferences prefs = getApplicationContext().getSharedPreferences(GameScreen.class.getSimpleName(),
                Context.MODE_PRIVATE);
        return prefs.getBoolean("FIRST_RUN", true);
    }
}
