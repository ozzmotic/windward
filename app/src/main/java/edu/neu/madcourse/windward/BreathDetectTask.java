package edu.neu.madcourse.windward;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

public class BreathDetectTask extends
        AsyncTask<BreathListener, Void, Boolean> {

    public static boolean isRecording = true;
    private static final String TAG = "RecordAmplitudeTask";

    public static int numberCreated = 0;
    private TextView status;
    private Context context;

    private static final String TEMP_AUDIO_DIR_NAME = "temp_audio";

    private static final int CLIP_TIME = 1000;

    public BreathDetectTask(Context context) {
        this.context = context;
        Log.i("create TASK", "CREATE TASK");

        numberCreated++;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(BreathListener... listeners) {
        if (listeners.length == 0) {

            Log.i("listener length", "listener length false");
            return false;
        }
        // construct recorder, using only the first listener passed in
        BreathListener listener = listeners[0];

        AudioClipRecorder recorder =
                new AudioClipRecorder(listener, this);

        boolean heard = false;

        //while (isRecording) {
            try {
                // start recording
                heard = recorder.startRecording();
                Log.i("alsd", "heard " + heard);
            } catch (IllegalStateException se) {
                Log.e(TAG, "failed to record, recorder not setup properly", se);
                heard = false;
            } catch (RuntimeException se) {
                Log.e(TAG, "failed to record, recorder already being used", se);
                heard = false;
            }
       // }

        return heard;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        // update UI
        if (result) {
            Log.i("heard", "heard from async task");
        } else {

        }

        super.onPostExecute(result);
    }

    @Override
    protected void onCancelled() {

        super.onCancelled();
        numberCreated = 0;
    }


}