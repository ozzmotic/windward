package edu.neu.madcourse.windward;

import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioRecord;
import android.media.AudioFormat;
import android.media.MediaRecorder.AudioSource;
import android.os.Handler;
import android.util.Log;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

//Root mean square
//Zero crossings
//Freq range

public class AudioManager extends Thread {

    private boolean stopped = false;
    public double volume = 0;
    public int freq = 0;
    public int freqRange;
    public float breathInc = 0;
    public float breathSpeed = 0.f;

    public int i_freq = 0;
    public double i_volume = 0;
    public int i_freqRange = 0;

    private LinkedList<Integer> frequencyHistory;

    private int freqRangeThreshold = 500;
    private int silenceThreshold = 500;
    private int historySize = 2;
    private int volumeThreshold = 500;
    private int breathBetweenTimeThreshold = 1000;
    private int freqThreshold = 500;
    private int breathTimeThreshold = 1000; //250

    private static final int DEFAULT_SILENCE_THRESHOLD = 2000;
    private static final int DEFAULT_BUFFER_INCREASE_FACTOR = 10;

    private boolean calBreathTime = true;

    private long prevIntervalTime = 0;
    private float interval = 1000;

    private final Handler myHandler = new Handler();
    public AudioRecord recorder = null;


    public AudioManager() {

        historySize = 2;
        freqRange = 0;
        frequencyHistory = new LinkedList<Integer>();
        for (int i = 0; i < historySize; i++) {
            frequencyHistory.add(Integer.MAX_VALUE);
        }

        silenceThreshold = DEFAULT_SILENCE_THRESHOLD;

        //createRecorder();
        android.os.Process
                .setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
        start();

        Timer myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                updateAudioData();
            }
        }, 0, 150);

        Log.i("AudioManager constructor", "audioman constructor");
    }

    public static boolean hasMicrophone(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_MICROPHONE);
    }

    public void setThresholds(int vol, int range, int betweenTime, int breathTime, int freq) {
        volumeThreshold = vol;
        freqRangeThreshold = range;
        freqThreshold = freq;
        breathBetweenTimeThreshold = betweenTime;
        breathTimeThreshold = breathTime;
    }

    private double rootMeanSquared(short[] nums) {
        double tempSum = 0;

        // Find rough idea of sound intensity
        for (int i = 0; i < nums.length; i++) {
            tempSum += nums[i] * nums[i];
        }

        tempSum /= nums.length;
        return Math.sqrt(tempSum);
    }

    public void createRecorder() {

        int N = AudioRecord.getMinBufferSize(44100,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT);
        Log.i("Create recorder", "create recorder");
        recorder = new AudioRecord(AudioSource.MIC, 44100,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT, N * DEFAULT_BUFFER_INCREASE_FACTOR);
    }

    @Override
    public void run() {

        recorder = null;
        Log.i("Audio", "Running Audio Thread");

        // AudioTrack track = null;
        short[][] buffers = new short[256][160];
        int ix = 0;

        // time length of breath
        long time = System.currentTimeMillis();
        long prevTime = 0;
        long breathTime = 0;
        long betweenTime = Long.MAX_VALUE;
        int N = 0;

        try {
            createRecorder();
            // track = new AudioTrack(AudioManager.STREAM_MUSIC, 8000,
            // AudioFormat.CHANNEL_OUT_MONO,
            // AudioFormat.ENCODING_PCM_16BIT, N * 10,
            // AudioTrack.MODE_STREAM);

            if (recorder != null) {
                recorder.startRecording();
                // track.play();

                while (!stopped) {
                    // Log.i("Map", "Writing new data to buffer");
                    short[] buffer = buffers[ix++ % buffers.length];
                    N = recorder.read(buffer, 0, buffer.length);

                    volume = rootMeanSquared(buffer);
                    //Log.i("Vol", volume + " ");

                    freq = calculateFreqFromZeroCrossing(44100, buffer);
                    //Log.i("ZC freq ", " " + freq);

                    calcFreqHistory(freq);
                    //Log.i("Freq Range ", " " + freqRange);

                    if (calBreathTime) {
                        if (i_volume > volumeThreshold && betweenTime > breathBetweenTimeThreshold) {

                            Log.i("Breath time", betweenTime + " " + i_volume);

                            // start timer only when input is within certain frequency range
                            if (i_freqRange < freqRangeThreshold && i_freq < freqThreshold) {
                                breathTime = (int) (System.currentTimeMillis() - prevTime);
                            }

                            // if within freq range for over 1 sec, confirm wind
                            if (breathTime > breathTimeThreshold) {
                                //Log.i("MIC", "vol: " + volume);
                                //Log.i("Breath time", betweenTime + " " + breathTime);

                                if (breathSpeed < 10) {
                                    breathSpeed += 1;//0.1f;
                                }

                                if (interval > 90) {
                                    interval -= breathSpeed;
                                }

                                if (System.currentTimeMillis() - prevIntervalTime > interval) {
                                    breathInc = breathInc + 1;
                                    //Log.i("move boat", "move boat" + " " + breathInc);
                                    prevIntervalTime = System.currentTimeMillis();
                                }

                            }

                            time = System.currentTimeMillis();

                        } else {

                            betweenTime = Math.abs(System.currentTimeMillis() - time);
                            prevTime = System.currentTimeMillis();
                            breathTime = 0;
                            breathSpeed = 0;
                            //breathInc = 0;

                            //ease out
                            if (interval < 1000) {
                                interval += 0.7f;

                                if (System.currentTimeMillis() - prevIntervalTime > interval) {
                                    breathInc = breathInc + 1;
                                    prevIntervalTime = System.currentTimeMillis();
                                }
                            }


                        }
                    }

                    // track.write(buffer, 0, buffer.length);
                }
            }

        } catch (Throwable x) {
            Log.i("thread run()", "run error");
        } finally {
            if (recorder != null) {
                recorder.stop();
                recorder.release();
            }

            Log.i("recorder stop", "recorder stopped and released");
            // track.stop();
            // track.release();
        }
    }

    private void close() {
        stopped = true;
    }

    public static int calculateFreqFromZeroCrossing(int sampleRate, short[] audioData) {
        int numSamples = audioData.length;
        int numCrossing = 0;
        for (int p = 0; p < numSamples - 1; p++) {
            if ((audioData[p] > 0 && audioData[p + 1] <= 0) || (audioData[p] < 0 && audioData[p + 1] >= 0)) {
                numCrossing++;
            }
        }
        float numSecondsRecorded = (float) numSamples / (float) sampleRate;
        float numCycles = numCrossing / 2;
        float frequency = numCycles / numSecondsRecorded;
        return (int) frequency;
    }

    //long freqHistoryPrevTime = 0;
    private void calcFreqHistory(int freq) {

        //clear frequencyHistory after x time
        /*
        if (System.currentTimeMillis() - freqHistoryPrevTime > 500) {
            frequencyHistory.clear();
            freqHistoryPrevTime = System.currentTimeMillis();
        }*/

        frequencyHistory.addFirst(freq);
        frequencyHistory.removeLast();

        //Log.i("history length "," " + frequencyHistory.size());

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (Integer val : frequencyHistory) {
            if (val >= max) {
                max = val;
            }
            if (val < min) {
                min = val;
            }
        }
        freqRange = max - min;


    }

    private void updateAudioData() {
        myHandler.post(myRunnable);
    }

    final Runnable myRunnable = new Runnable() {
        public void run() {
            i_freqRange = freqRange;
            i_volume = volume;
            i_freq = freq;
        }
    };

    public void setCalBreathTime(boolean calBreathTime) {
        this.calBreathTime = calBreathTime;
    }

    public void setStopped(boolean stopped) {
        this.stopped = stopped;
    }

    public boolean isStopped() {
        return stopped;
    }

    public boolean isCalBreathTime() {
        return calBreathTime;
    }
}