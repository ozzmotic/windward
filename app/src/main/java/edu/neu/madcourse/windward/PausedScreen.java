package edu.neu.madcourse.windward;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PausedScreen extends Activity {

    public static boolean doQuitFromPausedScreen = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_paused);

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/windlass.ttf");
        TextView pausedText = (TextView)findViewById(R.id.paused_text);
        pausedText.setTypeface(font);

        Button resumeButt = (Button)findViewById(R.id.button_resume);
        resumeButt.setTypeface(font);

        Button menuButt = (Button)findViewById(R.id.button_returntomenu);
        menuButt.setTypeface(font);

        Button helpButt = (Button)findViewById(R.id.button_help);
        helpButt.setTypeface(font);
    }

    public void resumeGame (View v) {
        finish();
        //Intent intent = new Intent(this, GameScreen.class);
        //intent.putExtra("NEW",false);
        //startActivity(intent);
    }

    public void quitGame (View v) {
        doQuitFromPausedScreen = true;
        finish();
        //Intent intent = new Intent(this, MainMenu.class);
        //startActivity(intent);
    }

    public void openHelpScreen(View v) {
        doQuitFromPausedScreen = true;
        finish();
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

}
